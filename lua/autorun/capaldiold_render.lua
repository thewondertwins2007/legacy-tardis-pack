if SERVER then
	hook.Add( "SetupPlayerVisibility", "capaldiold-Render", function(ply,viewent)
		if IsValid(ply.capaldiold) then
			AddOriginToPVS(ply.capaldiold:GetPos())
		end
	end)
elseif CLIENT then
	local rt,mat
	local size=1024
	local CamData = {}
	CamData.x = 0
	CamData.y = 0
	CamData.fov = 90
	CamData.drawviewmodel = false
	CamData.w = size
	CamData.h = size
	
	hook.Add("InitPostEntity", "capaldiold-Render", function()
		rt=GetRenderTarget("capaldiold_rt",size,size,false)
		mat=Material("models/vtalanov98old/capaldi/Scanner")
		mat:SetTexture("$basetexture",rt)
	end)
	
	hook.Add("RenderScene", "capaldiold-Render", function()
		if tobool(GetConVarNumber("capaldioldint_scanner"))==false then return end
		local capaldiold=LocalPlayer().capaldiold
		if IsValid(capaldiold) and LocalPlayer().capaldiold_viewmode then
			CamData.origin = capaldiold:LocalToWorld(Vector(24, 0, 80))
			CamData.angles = capaldiold:GetAngles()
			LocalPlayer().capaldiold_render=true
			local old = render.GetRenderTarget()
			render.SetRenderTarget( rt )
			render.Clear(0,0,0,255)
			cam.Start2D()
				render.RenderView(CamData)
			cam.End2D()
			render.CopyRenderTargetToTexture(rt)
			render.SetRenderTarget(old)
			LocalPlayer().capaldiold_render=false
		end
	end)
	
	hook.Add( "PreDrawHalos", "capaldiold-Render", function() // not ideal, but the new scanner sorta forced me to do this
		if tobool(GetConVarNumber("capaldioldint_halos"))==false then return end
		local capaldiold=LocalPlayer().capaldiold
		if IsValid(capaldiold) and not LocalPlayer().capaldiold_render then
			local Interior=capaldiold:GetNWEntity("Interior",NULL)
			if IsValid(Interior) and Interior.parts then
				for k,v in pairs(Interior.parts) do
					if v.shouldglow then
						halo.Add( {v}, Color( 255, 255, 255, 255 ), 1, 1, 1, true, true )
					end
				end
			end
		end
	end )
end