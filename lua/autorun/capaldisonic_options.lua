CreateClientConVar("capaldisonic_light_r", "0", true)
CreateClientConVar("capaldisonic_light_g", "100", true)
CreateClientConVar("capaldisonic_light_b", "250", true)

hook.Add("PopulateToolMenu", "capaldisonic-PopulateToolMenu", function()
	spawnmenu.AddToolMenuOption("Options", "Doctor Who (Legacy)", "capaldisonic_Options", "2015 Sonic v2.0", "", "", function(panel)
		panel:ClearControls()
		
		local Mixer1 = vgui.Create( "DColorMixer" )
		Mixer1:SetPalette( true )  		--Show/hide the palette			DEF:true
		Mixer1:SetAlphaBar( false ) 		--Show/hide the alpha bar		DEF:true
		Mixer1:SetWangs( true )	 		--Show/hide the R G B A indicators 	DEF:true
		Mixer1:SetColor( Color(GetConVarNumber("capaldisonic_light_r"), GetConVarNumber("capaldisonic_light_g"), GetConVarNumber("capaldisonic_light_b")) )	--Set the default color
		Mixer1.ValueChanged = function(self,col)
			RunConsoleCommand("capaldisonic_light_r", col.r)
			RunConsoleCommand("capaldisonic_light_g", col.g)
			RunConsoleCommand("capaldisonic_light_b", col.b)
		end
		panel:AddItem(Mixer1)
	end)
end)