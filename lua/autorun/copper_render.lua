if SERVER then
	hook.Add( "SetupPlayerVisibility", "copper-Render", function(ply,viewent)
		if IsValid(ply.copper) then
			AddOriginToPVS(ply.copper:GetPos())
		end
	end)
elseif CLIENT then
	local rt,mat
	local size=1024
	local CamData = {}
	CamData.x = 0
	CamData.y = 0
	CamData.fov = 90
	CamData.drawviewmodel = false
	CamData.w = size
	CamData.h = size
	
	hook.Add("InitPostEntity", "copper-Render", function()
		rt=GetRenderTarget("copper_rt",size,size,false)
		mat=Material("models/vtalanov98old/copper/Scanner")
		mat:SetTexture("$basetexture",rt)
	end)
	
	hook.Add("RenderScene", "copper-Render", function()
		if tobool(GetConVarNumber("copperint_scanner"))==false then return end
		local copper=LocalPlayer().copper
		if IsValid(copper) and LocalPlayer().copper_viewmode then
			CamData.origin = copper:LocalToWorld(Vector(24, 0, 80))
			CamData.angles = copper:GetAngles()
			LocalPlayer().copper_render=true
			local old = render.GetRenderTarget()
			render.SetRenderTarget( rt )
			render.Clear(0,0,0,255)
			cam.Start2D()
				render.RenderView(CamData)
			cam.End2D()
			render.CopyRenderTargetToTexture(rt)
			render.SetRenderTarget(old)
			LocalPlayer().copper_render=false
		end
	end)
	
	hook.Add( "PreDrawHalos", "copper-Render", function() // not ideal, but the new scanner sorta forced me to do this
		if tobool(GetConVarNumber("copperint_halos"))==false then return end
		local copper=LocalPlayer().copper
		if IsValid(copper) and not LocalPlayer().copper_render then
			local Interior=copper:GetNWEntity("Interior",NULL)
			if IsValid(Interior) and Interior.parts then
				for k,v in pairs(Interior.parts) do
					if v.shouldglow then
						halo.Add( {v}, Color( 255, 255, 255, 255 ), 1, 1, 1, true, true )
					end
				end
			end
		end
	end )
end