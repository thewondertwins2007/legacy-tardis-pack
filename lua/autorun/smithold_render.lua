if SERVER then
	hook.Add( "SetupPlayerVisibility", "smithold-Render", function(ply,viewent)
		if IsValid(ply.smithold) then
			AddOriginToPVS(ply.smithold:GetPos())
		end
	end)
elseif CLIENT then
	local rt,mat
	local size=1024
	local CamData = {}
	CamData.x = 0
	CamData.y = 0
	CamData.fov = 90
	CamData.drawviewmodel = false
	CamData.w = size
	CamData.h = size
	
	hook.Add("InitPostEntity", "smithold-Render", function()
		rt=GetRenderTarget("smithold_rt",size,size,false)
		mat=Material("models/vtalanov98old/smith/scanner")
		mat:SetTexture("$basetexture",rt)
	end)
	
	hook.Add("RenderScene", "smithold-Render", function()
		if tobool(GetConVarNumber("smitholdint_scanner"))==false then return end
		local smithold=LocalPlayer().smithold
		if IsValid(smithold) and LocalPlayer().smithold_viewmode then
			CamData.origin = smithold:LocalToWorld(Vector(24, 0, 80))
			CamData.angles = smithold:GetAngles()
			LocalPlayer().smithold_render=true
			local old = render.GetRenderTarget()
			render.SetRenderTarget( rt )
			render.Clear(0,0,0,255)
			cam.Start2D()
				render.RenderView(CamData)
			cam.End2D()
			render.CopyRenderTargetToTexture(rt)
			render.SetRenderTarget(old)
			LocalPlayer().smithold_render=false
		end
	end)
	
	hook.Add( "PreDrawHalos", "smithold-Render", function() // not ideal, but the new scanner sorta forced me to do this
		if tobool(GetConVarNumber("smitholdint_halos"))==false then return end
		local smithold=LocalPlayer().smithold
		if IsValid(smithold) and not LocalPlayer().smithold_render then
			local Interior=smithold:GetNWEntity("Interior",NULL)
			if IsValid(Interior) and Interior.parts then
				for k,v in pairs(Interior.parts) do
					if v.shouldglow then
						halo.Add( {v}, Color( 255, 255, 255, 255 ), 1, 1, 1, true, true )
					end
				end
			end
		end
	end )
end