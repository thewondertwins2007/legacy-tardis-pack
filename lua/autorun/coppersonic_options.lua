//2010 Sonic Screwdriver Spawnmenu Options

local checkbox_options={
	{"Sound", "coppersonic_sound"},
	{"Particle light", "coppersonic_light"},
	{"Dynamic light", "coppersonic_dynamiclight"},
}

for k,v in pairs(checkbox_options) do
	CreateClientConVar(v[2], "1", true)
end

CreateClientConVar("coppersonic_light_r", "0", true)
CreateClientConVar("coppersonic_light_g", "255", true)
CreateClientConVar("coppersonic_light_b", "0", true)

hook.Add("PopulateToolMenu", "coppersonic-PopulateToolMenu", function()
	spawnmenu.AddToolMenuOption("Options", "Doctor Who (Legacy)", "coppersonic_Options", "2010 Sonic Screwdriver", "", "", function(panel)
		panel:ClearControls()
		
		local Mixer1 = vgui.Create( "DColorMixer" )
		Mixer1:SetPalette( true )  		--Show/hide the palette			DEF:true
		Mixer1:SetAlphaBar( false ) 		--Show/hide the alpha bar		DEF:true
		Mixer1:SetWangs( true )	 		--Show/hide the R G B A indicators 	DEF:true
		Mixer1:SetColor( Color(GetConVarNumber("coppersonic_light_r"), GetConVarNumber("coppersonic_light_g"), GetConVarNumber("coppersonic_light_b")) )	--Set the default color
		Mixer1.ValueChanged = function(self,col)
			RunConsoleCommand("coppersonic_light_r", col.r)
			RunConsoleCommand("coppersonic_light_g", col.g)
			RunConsoleCommand("coppersonic_light_b", col.b)
		end
		panel:AddItem(Mixer1)
		
		local checkboxes={}
		for k,v in pairs(checkbox_options) do
			CreateClientConVar(v[2], "1", true)
			local checkBox = vgui.Create( "DCheckBoxLabel" ) 
			checkBox:SetText( v[1] ) 
			checkBox:SetValue( GetConVarNumber( v[2] ) )
			checkBox:SetConVar( v[2] )
			panel:AddItem(checkBox)
			table.insert(checkboxes, checkBox)
		end
	end)
end)