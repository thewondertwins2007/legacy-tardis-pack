if SERVER then
	hook.Add( "SetupPlayerVisibility", "hartnell-Render", function(ply,viewent)
		if IsValid(ply.hartnell) then
			AddOriginToPVS(ply.hartnell:GetPos())
		end
	end)
elseif CLIENT then
	local rt,mat
	local size=1024
	local CamData = {}
	CamData.x = 0
	CamData.y = 0
	CamData.fov = 90
	CamData.drawviewmodel = false
	CamData.w = size
	CamData.h = size
	
	hook.Add("InitPostEntity", "hartnell-Render", function()
		rt=GetRenderTarget("hartnell_rt",size,size,false)
		mat=Material("models/vtalanov98old/hartnell/scanner")
		mat:SetTexture("$basetexture",rt)
	end)
	
	hook.Add("RenderScene", "hartnell-Render", function()
		if tobool(GetConVarNumber("hartnellint_scanner"))==false then return end
		local hartnell=LocalPlayer().hartnell
		if IsValid(hartnell) and LocalPlayer().hartnell_viewmode then
			CamData.origin = hartnell:LocalToWorld(Vector(30, 0, 80))
			CamData.angles = hartnell:GetAngles()
			LocalPlayer().hartnell_render=true
			local old = render.GetRenderTarget()
			render.SetRenderTarget( rt )
			render.Clear(0,0,0,255)
			cam.Start2D()
				render.RenderView(CamData)
			cam.End2D()
			render.CopyRenderTargetToTexture(rt)
			render.SetRenderTarget(old)
			LocalPlayer().hartnell_render=false
		end
	end)
	
	hook.Add( "PreDrawHalos", "hartnell-Render", function() // not ideal, but the new scanner sorta forced me to do this
		if tobool(GetConVarNumber("hartnellint_halos"))==false then return end
		local hartnell=LocalPlayer().hartnell
		if IsValid(hartnell) and not LocalPlayer().hartnell_render then
			local interior=hartnell:GetNWEntity("interior",NULL)
			if IsValid(interior) and interior.parts then
				for k,v in pairs(interior.parts) do
					if v.shouldglow then
						halo.Add( {v}, Color( 255, 255, 255, 255 ), 1, 1, 1, true, true )
					end
				end
			end
		end
	end )
end