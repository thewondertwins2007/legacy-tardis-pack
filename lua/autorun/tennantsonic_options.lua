//Sonic Screwdriver Spawnmenu Options

local checkbox_options={
	{"Sound", "tennantsonic_sound"},
	{"Particle light", "tennantsonic_light"},
	{"Dynamic light", "tennantsonic_dynamiclight"},
}

for k,v in pairs(checkbox_options) do
	CreateClientConVar(v[2], "1", true)
end

CreateClientConVar("tennantsonic_light_r", "0", true)
CreateClientConVar("tennantsonic_light_g", "100", true)
CreateClientConVar("tennantsonic_light_b", "250", true)

hook.Add("PopulateToolMenu", "tennantsonic-PopulateToolMenu", function()
	spawnmenu.AddToolMenuOption("Options", "Doctor Who (Legacy)", "tennantsonic_Options", "2005 Sonic Screwdriver", "", "", function(panel)
		panel:ClearControls()
		
		local Mixer1 = vgui.Create( "DColorMixer" )
		Mixer1:SetPalette( true )  		--Show/hide the palette			DEF:true
		Mixer1:SetAlphaBar( false ) 		--Show/hide the alpha bar		DEF:true
		Mixer1:SetWangs( true )	 		--Show/hide the R G B A indicators 	DEF:true
		Mixer1:SetColor( Color(GetConVarNumber("tennantsonic_light_r"), GetConVarNumber("tennantsonic_light_g"), GetConVarNumber("tennantsonic_light_b")) )	--Set the default color
		Mixer1.ValueChanged = function(self,col)
			RunConsoleCommand("tennantsonic_light_r", col.r)
			RunConsoleCommand("tennantsonic_light_g", col.g)
			RunConsoleCommand("tennantsonic_light_b", col.b)
		end
		panel:AddItem(Mixer1)
		
		local checkboxes={}
		for k,v in pairs(checkbox_options) do
			CreateClientConVar(v[2], "1", true)
			local checkBox = vgui.Create( "DCheckBoxLabel" ) 
			checkBox:SetText( v[1] ) 
			checkBox:SetValue( GetConVarNumber( v[2] ) )
			checkBox:SetConVar( v[2] )
			panel:AddItem(checkBox)
			table.insert(checkboxes, checkBox)
		end
	end)
end)