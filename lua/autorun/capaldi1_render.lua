if SERVER then
	hook.Add( "SetupPlayerVisibility", "capaldi1-Render", function(ply,viewent)
		if IsValid(ply.capaldi1) then
			AddOriginToPVS(ply.capaldi1:GetPos())
		end
	end)
elseif CLIENT then
	local rt,mat
	local size=1024
	local CamData = {}
	CamData.x = 0
	CamData.y = 0
	CamData.fov = 90
	CamData.drawviewmodel = false
	CamData.w = size
	CamData.h = size
	
	hook.Add("InitPostEntity", "capaldi1-Render", function()
		rt=GetRenderTarget("capaldi1_rt",size,size,false)
		mat=Material("models/cem111333/capaldi1/screen")
		mat:SetTexture("$basetexture",rt)
	end)
	
	hook.Add("RenderScene", "capaldi1-Render", function()
		if tobool(GetConVarNumber("capaldi1int_scanner"))==false then return end
		local capaldi1=LocalPlayer().capaldi1
		if IsValid(capaldi1) and LocalPlayer().capaldi1_viewmode then
			CamData.origin = capaldi1:LocalToWorld(Vector(24, 0, 80))
			CamData.angles = capaldi1:GetAngles()
			LocalPlayer().capaldi1_render=true
			local old = render.GetRenderTarget()
			render.SetRenderTarget( rt )
			render.Clear(0,0,0,255)
			cam.Start2D()
				render.RenderView(CamData)
			cam.End2D()
			render.CopyRenderTargetToTexture(rt)
			render.SetRenderTarget(old)
			LocalPlayer().capaldi1_render=false
		end
	end)
	
	hook.Add( "PreDrawHalos", "capaldi1-Render", function() // not ideal, but the new scanner sorta forced me to do this
		if tobool(GetConVarNumber("capaldi1int_halos"))==false then return end
		local capaldi1=LocalPlayer().capaldi1
		if IsValid(capaldi1) and not LocalPlayer().capaldi1_render then
			local Interior=capaldi1:GetNWEntity("Interior",NULL)
			if IsValid(Interior) and Interior.parts then
				for k,v in pairs(Interior.parts) do
					if v.shouldglow then
						halo.Add( {v}, Color( 255, 255, 255, 255 ), 1, 1, 1, true, true )
					end
				end
			end
		end
	end )
end