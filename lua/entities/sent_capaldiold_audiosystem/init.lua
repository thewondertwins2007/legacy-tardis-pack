AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

util.AddNetworkString("capaldioldInt-Gramophone-GUI")
util.AddNetworkString("capaldioldInt-Gramophone-Bounce")
util.AddNetworkString("capaldioldInt-Gramophone-Send")

function ENT:Initialize()
	self:SetModel( "models/vtalanov98old/capaldi/audiosystem.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_NORMAL )
	self:SetColor(Color(255,255,255,255))
	self.phys = self:GetPhysicsObject()
	self:SetNWEntity("capaldiold",self.capaldiold)
	self.usecur=0
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
end

function ENT:Use( activator, caller, type, value )

	if ( !activator:IsPlayer() ) then return end		-- Who the frig is pressing this shit!?
	if IsValid(self.capaldiold) and ((self.capaldiold.isomorphic and not (activator==self.owner)) or not self.capaldiold.power) then
		return
	end
	
	local interior=self.interior
	local capaldiold=self.capaldiold
	if IsValid(self) and IsValid(interior) and IsValid(capaldiold) then
		interior.usecur=CurTime()+1
		if CurTime()>self.usecur then
			self.usecur=CurTime()+1
			net.Start("capaldioldInt-Gramophone-GUI")
				net.WriteEntity(self)
				net.WriteEntity(capaldiold)
				net.WriteEntity(interior)
			net.Send(activator)
		end
	end
	
	self:NextThink( CurTime() )
	return true
end

net.Receive("capaldioldInt-Gramophone-Bounce", function(l,ply)
	local gramophone=net.ReadEntity()
	local capaldiold=net.ReadEntity()
	local interior=net.ReadEntity()
	local play=tobool(net.ReadBit())
	local choice=net.ReadFloat()
	local custom=tobool(net.ReadBit())
	local customstr
	if custom then
		customstr=net.ReadString()
	end
	if IsValid(gramophone) and IsValid(capaldiold) and IsValid(interior) then
		net.Start("capaldioldInt-Gramophone-Send")
			net.WriteEntity(gramophone)
			net.WriteEntity(capaldiold)
			net.WriteEntity(interior)
			net.WriteBit(play)
			if play then
				net.WriteFloat(choice)
			end
			if custom then
				net.WriteBit(true)
				net.WriteString(customstr)
			else
				net.WriteBit(false)
			end
		net.Send(capaldiold.occupants)
	end
end)