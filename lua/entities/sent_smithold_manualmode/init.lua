AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

function ENT:Initialize()
	self:SetModel( "models/vtalanov98old/smith/telepathic.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_NORMAL )
	self:SetColor(Color(255,255,255,255))
	self.phys = self:GetPhysicsObject()
	self:SetNWEntity("smithold",self.smithold)
	self.usecur=0
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
end

function ENT:Use( activator, caller, type, value )

	if ( !activator:IsPlayer() ) then return end		-- Who the frig is pressing this shit!?
	if IsValid(self.smithold) and self.smithold.isomorphic and not (activator==self.owner) then
		return
	end
	
	local interior=self.interior
	if IsValid(interior) then
		interior.usecur=CurTime()+1
		if CurTime()>self.usecur then
			self.usecur=CurTime()+1
			if tobool(GetConVarNumber("smithold_advanced"))==true then
				if interior.flightmode==0 and interior.step==0 then
					local success=interior:StartAdv(1,activator)
					if success then
						activator:ChatPrint("Manual flightmode activated.")
					end
				else
					interior:UpdateAdv(activator,false)
				end
			end
		end
	end
	if IsValid(self.smithold) then
		net.Start("smitholdInt-ControlSound")
			net.WriteEntity(self.smithold)
			net.WriteEntity(self)
			net.WriteString("vtalanov98old/smith/manualmode.wav")
		net.Broadcast()
	end
	
	self:NextThink( CurTime() )
	return true
end