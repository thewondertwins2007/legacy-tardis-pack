include('shared.lua')

function ENT:Draw()
	if LocalPlayer().capaldiold==self:GetNWEntity("capaldiold", NULL) and LocalPlayer().capaldiold_viewmode and not LocalPlayer().capaldiold_render then
		self:DrawModel()
	end
end

function ENT:Initialize()
	self.PosePosition = 0.5
end

function ENT:Think()
	if LocalPlayer().capaldiold==self:GetNWEntity("capaldiold", NULL) and LocalPlayer().capaldiold_viewmode then
		local TargetPos = 0.0;
		if ( self:GetOn() ) then TargetPos = 1.0; end
		self.PosePosition = math.Approach( self.PosePosition, TargetPos, FrameTime() * 4 )
		self:SetPoseParameter( "switch", self.PosePosition )
		self:InvalidateBoneCache()
		

	end
end