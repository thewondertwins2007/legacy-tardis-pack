AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

util.AddNetworkString("capaldi1-SetViewmode")
util.AddNetworkString("capaldi1Int-SetParts")
util.AddNetworkString("capaldi1Int-UpdateAdv")
util.AddNetworkString("capaldi1Int-SetAdv")
util.AddNetworkString("capaldi1Int-ControlSound")

function ENT:Initialize()
	self:SetModel( "models/cem111333/capaldi1/interiorcap.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_TRANSALPHA )
	self:SetColor(Color(165,165,165,255))
	self:DrawShadow(false)
	
	self.phys = self:GetPhysicsObject()
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
	
	self:SetNWEntity("capaldi1",self.capaldi1)
	
	self.viewcur=0
	self.throttlecur=0
	self.usecur=0
	self.flightmode=0 //0 is none, 1 is skycamera selection, 2 is idk yet or whatever and so on
	self.step=0
	
	
	if WireLib then
		Wire_CreateInputs(self, { "Demat", "Phase", "Flightmode", "X", "Y", "Z", "XYZ [VECTOR]", "Rot" })
		Wire_CreateOutputs(self, { "Health" })
	end
	
	self:SpawnParts()
	
	if IsValid(self.owner) then
		self:SetNWVector("mainlight",Vector(self.owner:GetInfoNum("capaldi1int_mainlight_r",255),self.owner:GetInfoNum("capaldi1int_mainlight_g",50),self.owner:GetInfoNum("capaldi1int_mainlight_b",0)))
		self:SetNWVector("seclight",Vector(self.owner:GetInfoNum("capaldi1int_seclight_r",0),self.owner:GetInfoNum("capaldi1int_seclight_g",255),self.owner:GetInfoNum("capaldi1int_seclight_b",0)))
		self:SetNWVector("warnlight",Vector(self.owner:GetInfoNum("capaldi1int_warnlight_r",200),self.owner:GetInfoNum("capaldi1int_warnlight_g",0),self.owner:GetInfoNum("capaldi1int_warnlight_b",0)))
	end
end

function ENT:SpawnParts()
	if self.parts then
		for k,v in pairs(self.parts) do
			if IsValid(v) then
				v:Remove()
				v=nil
			end
		end
	end
	
	self.parts={}
	
	//chairs
	local vname="Seat_Airboat"
	local chair=list.Get("Vehicles")[vname]
	self.chair1=self:MakeVehicle(self:LocalToWorld(Vector(-145,-83,104)), Angle(0,-65,0), chair.Model, chair.Class, vname, chair)
	self.chair2=self:MakeVehicle(self:LocalToWorld(Vector(9,-165,104)), Angle(0,0,0), chair.Model, chair.Class, vname, chair)
	self.chair3=self:MakeVehicle(self:LocalToWorld(Vector(160,29,104)), Angle(0,100,0), chair.Model, chair.Class, vname, chair)
	self.chair4=self:MakeVehicle(self:LocalToWorld(Vector(43,155,104)), Angle(0,170,0), chair.Model, chair.Class, vname, chair)
	
	//parts
	self.audio=self:MakePart("sent_capaldi1_audio", Vector(0,0,0), Angle(0,0,0),false)
	self.books=self:MakePart("sent_capaldi1_books", Vector(0,0,0), Angle(0,0,0),false)
	self.console=self:MakePart("sent_capaldi1_console", Vector(0,0,0), Angle(0,0,0),false)
	self.door=self:MakePart("sent_capaldi1_door", Vector(0,0,0), Angle(0,90,0),false)
	self.doorframelights=self:MakePart("sent_capaldi1_doorframelights", Vector(0,0,0), Angle(0,90,0),false)
	self.floor=self:MakePart("sent_capaldi1_floor", Vector(0,0,0), Angle(0,0,0),false)
	self.lights=self:MakePart("sent_capaldi1_lights", Vector(0,0,0), Angle(0,0,0),false)
	self.roundelstone=self:MakePart("sent_capaldi1_roundelstone", Vector(0,0,0), Angle(0,0,0),false)
	self.neonorange=self:MakePart("sent_capaldi1_neonorange", Vector(0,0,0), Angle(0,0,0),false)
	self.lowertrim=self:MakePart("sent_capaldi1_lowertrim", Vector(0,0,0), Angle(0,0,0),false)
	self.trim=self:MakePart("sent_capaldi1_trim", Vector(0,0,0), Angle(0,0,0),false)
	self.catwalklights=self:MakePart("sent_capaldi1_catwalklights", Vector(0,0,0), Angle(0,0,0),false)
	self.catwalklightsfloor=self:MakePart("sent_capaldi1_catwalklightsfloor", Vector(0,0,0), Angle(0,0,0),false)
	self.catwalklightslow=self:MakePart("sent_capaldi1_catwalklightslow", Vector(0,0,0), Angle(0,90,0),false) 
	self.roundelstone=self:MakePart("sent_capaldi1_roundelstone", Vector(0,0,0), Angle(0,0,0),false)
	self.roundels=self:MakePart("sent_capaldi1_roundels", Vector(0,0,0), Angle(0,0,0),false)
	self.roundels=self:MakePart("sent_capaldi1_roundels", Vector(0,0,0), Angle(0,100,0),false)
	self.roundels=self:MakePart("sent_capaldi1_roundels", Vector(0,0,0), Angle(0,-100,0),false)
	self.hullstruts=self:MakePart("sent_capaldi1_hullstruts", Vector(0,0,0), Angle(0,-100,0),false)
	self.consoledetails=self:MakePart("sent_capaldi1_consoledetails", Vector(0,0,0), Angle(0,0,0),false)
	self.consolescanner=self:MakePart("sent_capaldi1_consolescanner", Vector(0,0,0), Angle(0,0,0),false)
	self.details=self:MakePart("sent_capaldi1_details", Vector(0,0,0), Angle(0,-0.075,0),false)
	self.details=self:MakePart("sent_capaldi1_details", Vector(0,0,0), Angle(0,198.975,0),false)
	self.rails=self:MakePart("sent_capaldi1_rails", Vector(0,0,0), Angle(0,0,0),false)
	self.manualmode=self:MakePart("sent_capaldi1_manualmode", Vector(0,0,0), Angle(0,0,0),false)
	self.rooms=self:MakePart("sent_capaldi1_rooms", Vector(0,0,0), Angle(0,0,0),false)
	self.intdoors=self:MakePart("sent_capaldi1_intdoors", Vector(0,0,0), Angle(0,0,0),false)
	self.power=self:MakePart("sent_capaldi1_power", Vector(0,0,0), Angle(0,0,0),false)
	self.coordinates=self:MakePart("sent_capaldi1_coordinates", Vector(0,0,0), Angle(0,0,0),false)
	self.throttle=self:MakePart("sent_capaldi1_throttle", Vector(0,0,0), Angle(0,0,0),false)
	self.screen=self:MakePart("sent_capaldi1_screen", Vector(0,0,0), Angle(0,0,0),false)
	self.screen1=self:MakePart("sent_capaldi1_screen1", Vector(0,0,0), Angle(0,0,0),false)
	self.screenring=self:MakePart("sent_capaldi1_screenring", Vector(0,0,0), Angle(0,0,0),false)
	self.skycamera=self:MakePart("sent_capaldi1_skycamera", Vector(0,0,-350), Angle(90,0,0),false)
	self.gears=self:MakePart("sent_capaldi1_gears", Vector(0,0,0), Angle(0,0,0),false)
	self.handbrake=self:MakePart("sent_capaldi1_handbrake", Vector(0,0,0), Angle(0,0,0),false)
	self.flightlever=self:MakePart("sent_capaldi1_flightlever", Vector(0,0,0), Angle(0,0,0),false)
	self.physbrake=self:MakePart("sent_capaldi1_physbrake", Vector(0,26.6,0), Angle(0,0,0),false)
	self.levers=self:MakePart("sent_capaldi1_levers", Vector(0,0,0), Angle(0,0,0),false)
	self.lever3=self:MakePart("sent_capaldi1_lever3", Vector(0,0,0), Angle(0,0,0),false)
	self.lever3=self:MakePart("sent_capaldi1_lever3", Vector(0,0,0), Angle(0,199,0),false)
	self.longflight=self:MakePart("sent_capaldi1_longflight", Vector(0,0,0), Angle(0,0,0),false)
	self.phasemode=self:MakePart("sent_capaldi1_phasemode", Vector(0,0,0), Angle(0,0,0),false)
	self.vortexmode=self:MakePart("sent_capaldi1_vortexmode", Vector(0,0,0), Angle(0,0,0),false)
	self.sliders=self:MakePart("sent_capaldi1_sliders", Vector(0,0,0), Angle(0,0,0),false)
	self.isomorphic=self:MakePart("sent_capaldi1_isomorphic", Vector(0,0,0), Angle(0,0,0),false)
	self.toggles2=self:MakePart("sent_capaldi1_toggles2", Vector(0,0,0), Angle(0,0,0),false)
	self.hads=self:MakePart("sent_capaldi1_hads", Vector(0,0,0), Angle(0,0,0),false)
	self.switches=self:MakePart("sent_capaldi1_switches", Vector(0,0,0), Angle(0,0,0),false)
	self.switches2=self:MakePart("sent_capaldi1_switches2", Vector(0,0,0), Angle(0,0,0),false)
	self.switches2=self:MakePart("sent_capaldi1_switches2", Vector(0,11.8,0), Angle(0,0,0),false)
	self.lock=self:MakePart("sent_capaldi1_lock", Vector(0,0,0), Angle(0,0,0),false)
	self.button2=self:MakePart("sent_capaldi1_button2", Vector(0,9.5,0), Angle(0,0,0),false)
	self.buttons=self:MakePart("sent_capaldi1_buttons", Vector(0,0,0), Angle(0,0,0),false)
	self.crank=self:MakePart("sent_capaldi1_crank", Vector(0,0,0), Angle(0,0,0),false)
	self.crank3=self:MakePart("sent_capaldi1_crank3", Vector(0,0,0), Angle(0,0,0),false)
	self.crank4=self:MakePart("sent_capaldi1_crank4", Vector(0,0,0), Angle(0,0,0),false)
	self.crank5=self:MakePart("sent_capaldi1_crank5", Vector(0,0,0), Angle(0,0,0),false)
	self.crank6=self:MakePart("sent_capaldi1_crank6", Vector(0,0,0), Angle(0,0,0),false)
	self.cranks=self:MakePart("sent_capaldi1_cranks", Vector(0,0,0), Angle(0,0,0),false)
	self.cranks=self:MakePart("sent_capaldi1_cranks", Vector(0,0,0), Angle(0,199,0),false)
	self.repair=self:MakePart("sent_capaldi1_repair", Vector(0,0,0), Angle(0,0,0),false)
	self.spinmode=self:MakePart("sent_capaldi1_spinmode", Vector(0,0,0), Angle(0,0,0),false)
	self.phone=self:MakePart("sent_capaldi1_phone", Vector(0,0,0), Angle(0,0,0),false)
	self.fastreturn=self:MakePart("sent_capaldi1_fastreturn", Vector(0,0,0), Angle(0,0,0),false)
	self.walls=self:MakePart("sent_capaldi1_walls", Vector(0,0,0), Angle(0,0,0),false)
	
	timer.Simple(2,function() // delay exists so the entity can register on the client, allows for a ping of just under 2000 (should be fine lol)
		if IsValid(self) and self.parts then
			net.Start("capaldi1Int-SetParts")
				net.WriteEntity(self)
				net.WriteFloat(#self.parts)
				for k,v in pairs(self.parts) do
					net.WriteEntity(v)
				end
			net.Broadcast()
		end
	end)
end

function ENT:StartAdv(mode,ply,pos,ang)
	if self.flightmode==0 and self.step==0 and IsValid(self.capaldi1) and self.capaldi1.power and not self.capaldi1.moving then
		self.flightmode=mode
		self.step=1
		if pos and ang then
			self.advpos=pos
			self.advang=ang
		end
		net.Start("capaldi1Int-SetAdv")
			net.WriteEntity(self)
			net.WriteEntity(ply)
			net.WriteFloat(mode)
		net.Send(ply)
		return true
	else
		return false
	end
end

function ENT:UpdateAdv(ply,success)
	if not (self.flightmode==0) and tobool(GetConVarNumber("capaldi1_advanced"))==true and IsValid(self.capaldi1) and self.capaldi1.power then
		if success then
			self.step=self.step+1
			if self.flightmode==1 and self.step==10 then
				local skycamera=self.skycamera
				if IsValid(self.capaldi1) and not self.capaldi1.moving and IsValid(skycamera) and skycamera.hitpos and skycamera.hitang then
					self.capaldi1:Go(skycamera.hitpos, skycamera.hitang)
					skycamera.hitpos=nil
					skycamera.hitang=nil
				else
					ply:ChatPrint("Error, already teleporting or no coordinates set.")
				end
				self.flightmode=0
				self.step=0
			elseif self.flightmode==2 and self.step==10 then
				if IsValid(self.capaldi1) and not self.capaldi1.moving and self.advpos and self.advpos then
					self.capaldi1:Go(self.advpos, self.advang)
				else
					ply:ChatPrint("Error, already teleporting or no coordinates set.")
				end
				self.advpos=nil
				self.advang=nil
				self.flightmode=0
				self.step=0
			elseif self.flightmode==3 and self.step==10 then
				local success=self.capaldi1:DematFast()
				if not success then
					ply:ChatPrint("Error, may be already teleporting.")
				end
				self.flightmode=0
				self.step=0
			end
		else
			//ply:ChatPrint("Failed.")
			self.flightmode=0
			self.step=0
			self.advpos=nil
			self.advang=nil
		end
		net.Start("capaldi1Int-UpdateAdv")
			net.WriteBit(success)
		net.Send(ply)
	end
end

function ENT:UpdateTransmitState()
	return TRANSMIT_ALWAYS
end

function ENT:MakePart(class,vec,ang,weld)
	local ent=ents.Create(class)
	ent.capaldi1=self.capaldi1
	ent.interior=self
	ent.owner=self.owner
	ent:SetPos(self:LocalToWorld(vec))
	ent:SetAngles(ang)
	//ent:SetCollisionGroup(COLLISION_GROUP_WORLD)
	ent:Spawn()
	ent:Activate()
	if weld then
		constraint.Weld(self,ent,0,0)
	end
	if IsValid(self.owner) then
		if SPropProtection then
			SPropProtection.PlayerMakePropOwner(self.owner, ent)
		else
			gamemode.Call("CPPIAssignOwnership", self.owner, ent)
		end
	end
	table.insert(self.parts,ent)
	return ent
end

function ENT:MakeVehicle( Pos, Ang, Model, Class, VName, VTable ) // for the chairs
	local ent = ents.Create( Class )
	if (!ent) then return NULL end
	
	ent:SetModel( Model )
	
	-- Fill in the keyvalues if we have them
	if ( VTable && VTable.KeyValues ) then
		for k, v in pairs( VTable.KeyValues ) do
			ent:SetKeyValue( k, v )
		end
	end
		
	ent:SetAngles( Ang )
	ent:SetPos( Pos )
		
	ent:Spawn()
	ent:Activate()
	
	ent.VehicleName 	= VName
	ent.VehicleTable 	= VTable
	
	-- We need to override the class in the case of the Jeep, because it 
	-- actually uses a different class than is reported by GetClass
	ent.ClassOverride 	= Class
	
	ent.capaldi1_part=true
	ent:GetPhysicsObject():EnableMotion(false)
	ent:SetRenderMode(RENDERMODE_TRANSALPHA)
	ent:SetColor(Color(255,255,255,0))
	constraint.Weld(self,ent,0,0)
	if IsValid(self.owner) then
		if SPropProtection then
			SPropProtection.PlayerMakePropOwner(self.owner, ent)
		else
			gamemode.Call("CPPIAssignOwnership", self.owner, ent)
		end
	end
	
	table.insert(self.parts,ent)

	return ent
end

if WireLib then
	function ENT:TriggerInput(k,v)
		if self.capaldi1 and IsValid(self.capaldi1) then
			self.capaldi1:TriggerInput(k,v)
		end
	end
end

function ENT:SetHP(hp)
	if WireLib then
		Wire_TriggerOutput(self, "Health", math.floor(hp))
	end
end

function ENT:Explode()
	self.exploded=true
	
	self.fire = ents.Create("env_fire_trail")
	self.fire:SetPos(self:LocalToWorld(Vector(0,0,0)))
	self.fire:Spawn()
	self.fire:SetParent(self)
	
	local explode = ents.Create("env_explosion")
	explode:SetPos(self:LocalToWorld(Vector(0,0,50)))
	explode:Spawn()
	explode:Fire("Explode",0)
	explode:EmitSound("cem111333/capaldi1/explosion.wav", 100, 100 ) //Adds sound to the explosion
end

function ENT:UnExplode()
	self.exploded=false
	
	if self.fire and IsValid(self.fire) then
		self.fire:Remove()
		self.fire=nil
	end
end

function ENT:OnRemove()
	if self.fire then
		self.fire:Remove()
		self.fire=nil
	end
	for k,v in pairs(self.parts) do
		if IsValid(v) then
			v:Remove()
			v=nil
		end
	end
end

function ENT:PlayerLookingAt(ply,vec,fov,Width)	
	local Disp = vec - self:WorldToLocal(ply:GetPos()+Vector(0,0,100))
	local Dist = Disp:Length()
	
	local MaxCos = math.abs( math.cos( math.acos( Dist / math.sqrt( Dist * Dist + Width * Width ) ) + fov * ( math.pi / 50 ) ) )
	Disp:Normalize()
	
	if Disp:Dot( ply:EyeAngles():Forward() ) > MaxCos then
		return true
	end
	
    return false
end

function ENT:Use( ply )
	if CurTime()>self.usecur and self.capaldi1 and IsValid(self.capaldi1) and ply.capaldi1 and IsValid(ply.capaldi1) and ply.capaldi1==self.capaldi1 and ply.capaldi1_viewmode and not ply.capaldi1_skycamera then

		//this must go last, or bad things may happen
		if CurTime()>self.capaldi1.viewmodecur then
			local pos=Vector(0,0,100)
			local pos2=self:WorldToLocal(ply:GetPos())
			local distance=pos:Distance(pos2)
			if distance < 110 and self:PlayerLookingAt(ply, Vector(0,0,100), 25, 25) then
				self.capaldi1:ToggleViewmode(ply)
				self.usecur=CurTime()+1
				self.capaldi1.viewmodecur=CurTime()+1
				return
			end
		end
	end
end

function ENT:OnTakeDamage(dmginfo)
	if self.capaldi1 and IsValid(self.capaldi1) then
		self.capaldi1:OnTakeDamage(dmginfo)
	end
end

function ENT:Think()
	if self.capaldi1 and IsValid(self.capaldi1) then
		if self.capaldi1.occupants then
			for k,v in pairs(self.capaldi1.occupants) do
				if self:GetPos():Distance(v:GetPos()) > 700 and v.capaldi1_viewmode and not v.capaldi1_skycamera then
					self.capaldi1:PlayerExit(v,true)
				end
			end
		end
	end
end