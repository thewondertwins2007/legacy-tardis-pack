include('shared.lua')
 
--[[---------------------------------------------------------
   Name: Draw
   Purpose: Draw the model in-game.
   Remember, the things you render first will be underneath!
---------------------------------------------------------]]
function ENT:Draw()
	if LocalPlayer().capaldi1_viewmode and self:GetNWEntity("capaldi1",NULL)==LocalPlayer().capaldi1 and not LocalPlayer().capaldi1_render then
		self:DrawModel()
		if WireLib then
			Wire_Render(self)
		end
	end
end

function ENT:OnRemove()
	if self.cloisterbell then
		self.cloisterbell:Stop()
		self.cloisterbell=nil
	end
	if self.creaks then
		self.creaks:Stop()
		self.creaks=nil
	end
	if self.idlesound then
		self.idlesound:Stop()
		self.idlesound=nil
	end
	if self.crack then
		self.crack:Stop()
		self.creaks=nil
	end
end

function ENT:Initialize()
	self.timerotor={}
	self.timerotor.pos=0
	self.timerotor.mode=1
	self.parts={}
end

net.Receive("capaldi1Int-SetParts", function()
	local t={}
	local interior=net.ReadEntity()
	local count=net.ReadFloat()
	for i=1,count do
		local ent=net.ReadEntity()
		ent.capaldi1_part=true
		if IsValid(interior) then
			table.insert(interior.parts,ent)
		end
	end
end)

net.Receive("capaldi1Int-UpdateAdv", function()
	local success=tobool(net.ReadBit())
	if success then
	else
		surface.PlaySound("cem111333/capaldi1/cloisterbell.wav")
	end
end)

net.Receive("capaldi1Int-SetAdv", function()
	local interior=net.ReadEntity()
	local ply=net.ReadEntity()
	local mode=net.ReadFloat()
	if IsValid(interior) and IsValid(ply) and mode then
		if ply==LocalPlayer() then
		end
		interior.flightmode=mode
	end
end)

net.Receive("capaldi1Int-ControlSound", function()
	local capaldi1=net.ReadEntity()
	local control=net.ReadEntity()
	local snd=net.ReadString()
	if IsValid(capaldi1) and IsValid(control) and snd and tobool(GetConVarNumber("capaldi1int_controlsound"))==true and LocalPlayer().capaldi1==capaldi1 and LocalPlayer().capaldi1_viewmode then
		sound.Play(snd,control:GetPos())
	end
end)

function ENT:Think()
	local capaldi1=self:GetNWEntity("capaldi1",NULL)
	if IsValid(capaldi1) and LocalPlayer().capaldi1_viewmode and LocalPlayer().capaldi1==capaldi1 then
		if tobool(GetConVarNumber("capaldi1int_cloisterbell"))==true and not IsValid(LocalPlayer().capaldi1_skycamera) then
			if capaldi1.health and capaldi1.health < 21 then
				if self.cloisterbell and !self.cloisterbell:IsPlaying() then
					self.cloisterbell:Play()
				elseif not self.cloisterbell then
					self.cloisterbell = CreateSound(self, "cem111333/capaldi1/cloisterbell_loop.wav")
					self.cloisterbell:Play()
				end
			else
				if self.cloisterbell and self.cloisterbell:IsPlaying() then
					self.cloisterbell:Stop()
					self.cloisterbell=nil
				end
			end
		else
			if self.cloisterbell and self.cloisterbell:IsPlaying() then
				self.cloisterbell:Stop()
				self.cloisterbell=nil
			end
		end

		if tobool(GetConVarNumber("capaldi1int_creaks"))==true and not IsValid(LocalPlayer().capaldi1_skycamera) then
			if not capaldi1.power or capaldi1.repairing then
				if self.creaks and !self.creaks:IsPlaying() then
					self.creaks:Play()
				elseif not self.creaks then
					self.creaks = CreateSound(self, "cem111333/capaldi1/creaks_loop.wav")
					self.creaks:Play()
				        self.creaks:ChangeVolume(0.4,0)
				end
			else
				if self.creaks and self.creaks:IsPlaying() then
					self.creaks:Stop()
					self.creaks=nil
				end
			end
		else
			if self.creaks and self.creaks:IsPlaying() then
				self.creaks:Stop()
				self.creaks=nil
			end
		end
		if tobool(GetConVarNumber("capaldi1int_crack"))==true and not IsValid(LocalPlayer().capaldi1_skycamera) then
			if capaldi1.health and capaldi1.health < 11 then
				if self.crack and !self.crack:IsPlaying() then
					self.crack:Play()
				elseif not self.crack then
					self.crack = CreateSound(self, "cem111333/capaldi1/crack.wav")
					self.crack:Play()
				        self.crack:ChangeVolume(1,0)
				end
			else
				if self.crack and self.crack:IsPlaying() then
					self.crack:Stop()
					self.crack=nil
				end
			end
		else
			if self.crack and self.crack:IsPlaying() then
				self.crack:Stop()
				self.crack=nil
			end
		end	
		
		if tobool(GetConVarNumber("capaldi1int_idlesound"))==true and capaldi1.health and capaldi1.health >= 1 and not IsValid(LocalPlayer().capaldi1_skycamera) and not capaldi1.repairing and capaldi1.power then
			if self.idlesound and !self.idlesound:IsPlaying() then
				self.idlesound:Play()
			elseif not self.idlesound then
				self.idlesound = CreateSound(self, "cem111333/capaldi1/interior_idle_loop.wav")
				self.idlesound:Play()
				self.idlesound:ChangeVolume(0.7,0)
			end
		else
			if self.idlesound and self.idlesound:IsPlaying() then
				self.idlesound:Stop()
				self.idlesound=nil
			end
		end
		
		if not IsValid(LocalPlayer().capaldi1_skycamera) and tobool(GetConVarNumber("capaldi1int_dynamiclight"))==true then
			if capaldi1.health and capaldi1.health > 0 and not capaldi1.repairing and capaldi1.power then
				local dlight = DynamicLight( self:EntIndex() )
				if ( dlight ) then
					local size=1024
					local v=self:GetNWVector("mainlight",Vector(0,0,0))
					dlight.Pos = self:LocalToWorld(Vector(0,0,300))
					dlight.r = v.x
					dlight.g = v.y
					dlight.b = v.z
					dlight.Brightness = 3
					dlight.Decay = size * 5
					dlight.Size = size
					dlight.DieTime = CurTime() + 1
				end
			end

			if capaldi1.health and capaldi1.health > 0 and not capaldi1.repairing and capaldi1.power then
			   local dlight2 = DynamicLight( self:EntIndex()+10000 )
			   if ( dlight2 ) then
				   local size=1024
				   local v=self:GetNWVector("seclight",Vector(0,0,0))
					if capaldi1.health < 21 then
						v=self:GetNWVector("warnlight",Vector(0,0,0))
					end
				   dlight2.Pos = self:LocalToWorld(Vector(0,0,-50))
				   dlight2.r = v.x
				   dlight2.g = v.y
				   dlight2.b = v.z
				   dlight2.Brightness = 4
				   dlight2.Decay = size * 5
				   dlight2.Size = size
				   dlight2.DieTime = CurTime() + 1
			   end
                        end
			
			if (capaldi1.moving or capaldi1.flightmode) then
				if self.timerotor.pos==1 then
					self.timerotor.pos=0
				end
				
				self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.07 )
				self:SetPoseParameter( "rotor", self.timerotor.pos )
			end
              end
	else
		if self.cloisterbell then
			self.cloisterbell:Stop()
			self.cloisterbell=nil
		end
		if self.creaks then
			self.creaks:Stop()
			self.creaks=nil
		end
		if self.crack then
			self.crack:Stop()
			self.crack=nil
		end
		if self.idlesound then
			self.idlesound:Stop()
			self.idlesound=nil
		end
	end
end