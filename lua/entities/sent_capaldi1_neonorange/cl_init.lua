include('shared.lua')

function ENT:Draw()
	if LocalPlayer().capaldi1==self:GetNWEntity("capaldi1", NULL) and LocalPlayer().capaldi1_viewmode and not LocalPlayer().capaldi1_render then
		self:DrawModel()
	end
end

function ENT:Initialize()
	self.PosePosition = 0.5
end

function ENT:Think()
	local capaldi1=self:GetNWEntity("capaldi1",NULL)
	if IsValid(capaldi1) and LocalPlayer().capaldi1_viewmode and LocalPlayer().capaldi1==capaldi1 then
			if capaldi1.health and capaldi1.health > 0 and not capaldi1.repairing and capaldi1.power then
	                          self:SetColor(Color(255,255,255))
                        else
                                  self:SetColor(Color(0,0,0))
			end
        end
end