include('shared.lua')
 
--[[---------------------------------------------------------
   Name: Draw
   Purpose: Draw the model in-game.
   Remember, the things you render first will be underneath!
---------------------------------------------------------]]
function ENT:Draw() 
	if not self.phasing and self.visible and not self.invortex then
		self:DrawModel()
		if WireLib then
			Wire_Render(self)
		end
	elseif self.phasing then
		if self.percent then
			if not self.phasemode and self.highPer <= 0 then
				self.phasing=false
			elseif self.phasemode and self.percent >= 1 then
				self.phasing=false
			end
		end
		
		self.percent = (self.phaselifetime - CurTime())
		self.highPer = self.percent + 0.5
		if self.phasemode then
			self.percent = (1-self.percent)-0.5
			self.highPer = self.percent+0.5
		end
		self.percent = math.Clamp( self.percent, 0, 1 )
		self.highPer = math.Clamp( self.highPer, 0, 1 )

		--Drawing original model
		local normal = self:GetUp()
		local origin = self:GetPos() + self:GetUp() * (self.maxs.z - ( self.height * self.highPer ))
		local distance = normal:Dot( origin )
		
		render.EnableClipping( true )
		render.PushCustomClipPlane( normal, distance )
			self:DrawModel()
		render.PopCustomClipPlane()
		
		local restoreT = self:GetMaterial()
		
		--Drawing phase texture
		render.MaterialOverride( self.wiremat )

		normal = self:GetUp()
		distance = normal:Dot( origin )
		render.PushCustomClipPlane( normal, distance )
		
		local normal2 = self:GetUp() * -1
		local origin2 = self:GetPos() + self:GetUp() * (self.maxs.z - ( self.height * self.percent ))
		local distance2 = normal2:Dot( origin2 )
		render.PushCustomClipPlane( normal2, distance2 )
			self:DrawModel()
		render.PopCustomClipPlane()
		render.PopCustomClipPlane()
		
		render.MaterialOverride( restoreT )
		render.EnableClipping( false )
	end
end

function ENT:Phase(mode)
	self.phasing=true
	self.phaseactive=true
	self.phaselifetime=CurTime()+1
	self.phasemode=mode
end

function ENT:Initialize()
	self.health=100
	self.phasemode=false
	self.visible=true
	self.flightmode=false
	self.visible=true
	self.power=true
	self.z=0
	self.phasedraw=0
	self.mins = self:OBBMins()
	self.maxs = self:OBBMaxs()
	self.wiremat = Material( "models/cem111333/capaldi1/phase" )
	self.height = self.maxs.z - self.mins.z
end

function ENT:OnRemove()
	if self.flightloop then
		self.flightloop:Stop()
		self.flightloop=nil
	end
	if self.flightloop2 then
		self.flightloop2:Stop()
		self.flightloop2=nil
	end
end

function ENT:Think()
	if tobool(GetConVarNumber("capaldi1_flightsound"))==true then
		if not self.flightloop then
			self.flightloop=CreateSound(self, "cem111333/capaldi1/flight_loopext.wav")
			self.flightloop:SetSoundLevel(90)
			self.flightloop:Stop()
		end
		if self.flightmode and self.visible and not self.moving then
			if !self.flightloop:IsPlaying() then
				self.flightloop:Play()
			end
			local e = LocalPlayer():GetViewEntity()
			if !IsValid(e) then e = LocalPlayer() end
			local capaldi1=LocalPlayer().capaldi1
			if not (capaldi1 and IsValid(capaldi1) and capaldi1==self and e==LocalPlayer()) then
				local pos = e:GetPos()
				local spos = self:GetPos()
				local doppler = (pos:Distance(spos+e:GetVelocity())-pos:Distance(spos+self:GetVelocity()))/200
				if self.exploded then
					local r=math.random(90,130)
					self.flightloop:ChangePitch(math.Clamp(r+doppler,80,120),0.1)
				else
					self.flightloop:ChangePitch(math.Clamp((self:GetVelocity():Length()/250)+95+doppler,80,120),0.1)
				end
				self.flightloop:ChangeVolume(GetConVarNumber("capaldi1_flightvol"),0)
			else
				if self.exploded then
					local r=math.random(90,130)
					self.flightloop:ChangePitch(r,0.1)
				else
					local p=math.Clamp(self:GetVelocity():Length()/250,0,15)
					self.flightloop:ChangePitch(95+p,0.1)
				end
				self.flightloop:ChangeVolume(0.75*GetConVarNumber("capaldi1_flightvol"),0)
			end
		else
			if self.flightloop:IsPlaying() then
				self.flightloop:Stop()
			end
		end
		
		local interior=self:GetNWEntity("interior",NULL)
		if not self.flightloop2 and interior and IsValid(interior) then
			self.flightloop2=CreateSound(interior, "cem111333/capaldi1/flight_loop.wav")
			self.flightloop2:Stop()
		end
		if self.flightloop2 and (self.flightmode or self.invortex) and LocalPlayer().capaldi1_viewmode and not IsValid(LocalPlayer().capaldi1_skycamera) and interior and IsValid(interior) and ((self.invortex and self.moving) or not self.moving) then
			if !self.flightloop2:IsPlaying() then
				self.flightloop2:Play()
				self.flightloop2:ChangeVolume(0.8,0)
			end
			if self.exploded then
				local r=math.random(90,130)
			else
				local p=math.Clamp(self:GetVelocity():Length()/250,0,15)
			end
		elseif self.flightloop2 then
			if self.flightloop2:IsPlaying() then
				self.flightloop2:Stop()
			end
		end
	else
		if self.flightloop then
			self.flightloop:Stop()
			self.flightloop=nil
		end
		if self.flightloop2 then
			self.flightloop2:Stop()
			self.flightloop2=nil
		end
	end
	
	if self.light_on and tobool(GetConVarNumber("capaldi1_dynamiclight"))==true then
		local dlight = DynamicLight( self:EntIndex() )
		if ( dlight ) then
			local size=400
			local v=self:GetNWVector("extcol",Vector(255,255,255))
			local c=Color(v.x,v.y,v.z)
			dlight.Pos = self:GetPos() + self:GetUp() * 123
			dlight.r = c.r
			dlight.g = c.g
			dlight.b = c.b
			dlight.Brightness = 1
			dlight.Decay = size * 5
			dlight.Size = size
			dlight.DieTime = CurTime() + 1
		end
	end
	if self.health and self.health > 20 and self.visible and self.power and not self.moving and tobool(GetConVarNumber("capaldi1_dynamiclight"))==true then
		local dlight2 = DynamicLight( self:EntIndex() )
		if ( dlight2 ) then
			local size=400
			local v=self:GetNWVector("extcol",Vector(255,255,255))
			local c=Color(v.x,v.y,v.z)
			dlight2.Pos = self:GetPos() + self:GetUp() * 123
			dlight2.r = c.r
			dlight2.g = c.g
			dlight2.b = c.b
			dlight2.Brightness = 1
			dlight2.Decay = size * 5
			dlight2.Size = size
			dlight2.DieTime = CurTime() + 1
		end
	end
end

net.Receive("capaldi1-UpdateVis", function()
	local ent=net.ReadEntity()
	ent.visible=tobool(net.ReadBit())
end)

net.Receive("capaldi1-Phase", function()
	local capaldi1=net.ReadEntity()
	local interior=net.ReadEntity()
	if IsValid(capaldi1) then
		capaldi1.visible=tobool(net.ReadBit())
		capaldi1:Phase(capaldi1.visible)
		if not capaldi1.visible and tobool(GetConVarNumber("capaldi1_phasesound"))==true then
			capaldi1:EmitSound("cem111333/capaldi1/cloak.wav", 100, 100)
			if IsValid(interior) then
				interior:EmitSound("cem111333/capaldi1/cloak.wav", 100, 100)
			end
		else
			capaldi1:EmitSound("cem111333/capaldi1/uncloak.wav", 100, 100)
			if IsValid(interior) then
				interior:EmitSound("cem111333/capaldi1/uncloak.wav", 100, 100)
			end
		end
	end
end)

net.Receive("capaldi1-Explode", function()
	local ent=net.ReadEntity()
	ent.exploded=true
end)

net.Receive("capaldi1-UnExplode", function()
	local ent=net.ReadEntity()
	ent.exploded=false
end)

net.Receive("capaldi1-Flightmode", function()
	local ent=net.ReadEntity()
	ent.flightmode=tobool(net.ReadBit())
end)

net.Receive("capaldi1-SetInterior", function()
	local ent=net.ReadEntity()
	ent.interior=net.ReadEntity()
end)

local tpsounds={}
tpsounds[0]={ // normal
	"cem111333/capaldi1/dematext.wav",
	"cem111333/capaldi1/matext.wav",
	"cem111333/capaldi1/fullext.wav"
}
tpsounds[1]={ // fast demat
	"cem111333/capaldi1/fast_demat.wav",
	"cem111333/capaldi1/matext.wav",
	"cem111333/capaldi1/fullext.wav"
}
tpsounds[2]={ // fast return
	"cem111333/capaldi1/fastreturn_demat.wav",
	"cem111333/capaldi1/fastreturn_mat.wav",
	"cem111333/capaldi1/fastreturn_full.wav"
}
net.Receive("capaldi1-Go", function()
	local capaldi1=net.ReadEntity()
	if IsValid(capaldi1) then
		capaldi1.moving=true
	end
	local interior=net.ReadEntity()
	local exploded=tobool(net.ReadBit())
	local pitch=(exploded and 110 or 100)
	local long=tobool(net.ReadBit())
	local snd=net.ReadFloat()
	local snds=tpsounds[snd]
	if tobool(GetConVarNumber("capaldi1_matsound"))==true then
		if IsValid(capaldi1) and LocalPlayer().capaldi1==capaldi1 then
			if capaldi1.visible then
				if long then
					capaldi1:EmitSound(snds[1], 100, pitch)
				else
					capaldi1:EmitSound(snds[3], 100, pitch)
				end
			end
			if interior and IsValid(interior) and LocalPlayer().capaldi1_viewmode and not IsValid(LocalPlayer().capaldi1_skycamera) then
				if long then
					sound.Play("cem111333/capaldi1/demat.wav", interior:LocalToWorld(Vector(0,0,0)))
				else
					sound.Play("cem111333/capaldi1/full.wav", interior:LocalToWorld(Vector(0,0,0)))
				end
			end
		elseif IsValid(capaldi1) and capaldi1.visible then
			local pos=net.ReadVector()
			local pos2=net.ReadVector()
			if pos then
				sound.Play(snds[1], pos, 75, pitch)
			end
			if pos2 and not long then
				sound.Play(snds[2], pos2, 75, pitch)
			end
		end
	end
end)

net.Receive("capaldi1-Stop", function()
	capaldi1=net.ReadEntity()
	if IsValid(capaldi1) then
		capaldi1.moving=nil
	end
end)

net.Receive("capaldi1-Reappear", function()
	local capaldi1=net.ReadEntity()
	local interior=net.ReadEntity()
	local exploded=tobool(net.ReadBit())
	local pitch=(exploded and 110 or 100)
	local snd=net.ReadFloat()
	local snds=tpsounds[snd]
	if tobool(GetConVarNumber("capaldi1_matsound"))==true then
		if IsValid(capaldi1) and LocalPlayer().capaldi1==capaldi1 then
			if capaldi1.visible then
				capaldi1:EmitSound(snds[2], 100, pitch)
			end
			if interior and IsValid(interior) and LocalPlayer().capaldi1_viewmode and not IsValid(LocalPlayer().capaldi1_skycamera) then
				sound.Play("cem111333/capaldi1/mat.wav", interior:LocalToWorld(Vector(0,0,0)))
			end
		elseif IsValid(capaldi1) and capaldi1.visible then
			sound.Play(snds[2], net.ReadVector(), 75, pitch)
		end
	end
end)

net.Receive("Player-Setcapaldi1", function()
	local ply=net.ReadEntity()
	ply.capaldi1=net.ReadEntity()
end)

net.Receive("capaldi1-SetHealth", function()
	local capaldi1=net.ReadEntity()
	capaldi1.health=net.ReadFloat()
end)

net.Receive("capaldi1-SetLocked", function()
	local capaldi1=net.ReadEntity()
	local interior=net.ReadEntity()
	local locked=tobool(net.ReadBit())
	local makesound=tobool(net.ReadBit())
	if IsValid(capaldi1) then
		capaldi1.locked=locked
		if tobool(GetConVarNumber("capaldi1_locksound"))==true and makesound then
			sound.Play("cem111333/capaldi1/lock.wav", capaldi1:GetPos())
		end
	end
	if IsValid(interior) then
		if tobool(GetConVarNumber("capaldi1_locksound"))==true and not IsValid(LocalPlayer().capaldi1_skycamera) and makesound then
			sound.Play("cem111333/capaldi1/lock.wav", interior:LocalToWorld(Vector(-308,0,0)))
		end
	end
end)

net.Receive("capaldi1-SetViewmode", function()
	LocalPlayer().capaldi1_viewmode=tobool(net.ReadBit())
	LocalPlayer().ShouldDisableLegs=(not LocalPlayer().capaldi1_viewmode)
	
	if LocalPlayer().capaldi1_viewmode and GetConVarNumber("r_rootlod")>0 then
		Derma_Query("The TARDIS Interior requires model detail on high, set now?", "2017 Tardis Interior", "Yes", function() RunConsoleCommand("r_rootlod", 0) end, "No", function() end)
	end
		
end)

hook.Add( "ShouldDrawLocalPlayer", "capaldi1-ShouldDrawLocalPlayer", function(ply)
	if IsValid(ply.capaldi1) and not ply.capaldi1_viewmode then
		return false
	end
end)

net.Receive("capaldi1-PlayerEnter", function()
	if tobool(GetConVarNumber("capaldi1_doorsound"))==true then
		local ent1=net.ReadEntity()
		local ent2=net.ReadEntity()
		if IsValid(ent1) and ent1.visible then
			sound.Play("cem111333/capaldi1/door.wav", ent1:GetPos())
		end
		if IsValid(ent2) and not IsValid(LocalPlayer().capaldi1_skycamera) then
			sound.Play("cem111333/capaldi1/door.wav", ent2:LocalToWorld(Vector(-308,0,0)))
		end
	end
end)

net.Receive("capaldi1-PlayerExit", function()
	if tobool(GetConVarNumber("capaldi1_doorsound"))==true then
		local ent1=net.ReadEntity()
		local ent2=net.ReadEntity()
		if IsValid(ent1) and ent1.visible then
			sound.Play("cem111333/capaldi1/door.wav", ent1:GetPos())
		end
		if IsValid(ent2) and not IsValid(LocalPlayer().capaldi1_skycamera) then
			sound.Play("cem111333/capaldi1/door.wav", ent2:LocalToWorld(Vector(-308,0,0)))
		end
	end
end)

net.Receive("capaldi1-SetRepairing", function()
	local capaldi1=net.ReadEntity()
	local repairing=tobool(net.ReadBit())
	local interior=net.ReadEntity()
	if IsValid(capaldi1) then
		capaldi1.repairing=repairing
	end
	if IsValid(interior) and LocalPlayer().capaldi1==capaldi1 and LocalPlayer().capaldi1_viewmode and tobool(GetConVarNumber("capaldi1int_powersound"))==true then
		if repairing then
			sound.Play("cem111333/capaldi1/powerdown.wav", interior:GetPos())
		else
			sound.Play("cem111333/capaldi1/powerup.wav", interior:GetPos())
		end
	end
end)

net.Receive("capaldi1-BeginRepair", function()
	local capaldi1=net.ReadEntity()
	if IsValid(capaldi1) then
		/*
		local mat=Material("models/drmatt/capaldi1/capaldi1_df")
		if not mat:IsError() then
			mat:SetTexture("$basetexture", "models/props_combine/metal_combinebridge001")
		end
		*/
	end
end)

net.Receive("capaldi1-FinishRepair", function()
	local capaldi1=net.ReadEntity()
	if IsValid(capaldi1) then
		if tobool(GetConVarNumber("capaldi1int_repairsound"))==true and capaldi1.visible then
			sound.Play("cem111333/capaldi1/repairfinish.wav", capaldi1:GetPos())
		end
		/*
		local mat=Material("models/drmatt/capaldi1/capaldi1_df")
		if not mat:IsError() then
			mat:SetTexture("$basetexture", "models/drmatt/capaldi1/capaldi1_df")
		end
		*/
	end
end)

net.Receive("capaldi1-SetLight", function()
	local capaldi1=net.ReadEntity()
	local on=tobool(net.ReadBit())
	if IsValid(capaldi1) then
		capaldi1.light_on=on
	end
end)

net.Receive("capaldi1-SetPower", function()
	local capaldi1=net.ReadEntity()
	local on=tobool(net.ReadBit())
	local interior=net.ReadEntity()
	if IsValid(capaldi1) then
		capaldi1.power=on
	end
	if IsValid(interior) and LocalPlayer().capaldi1==capaldi1 and LocalPlayer().capaldi1_viewmode and tobool(GetConVarNumber("capaldi1int_powersound"))==true then
		if on then
			sound.Play("cem111333/capaldi1/powerup.wav", interior:GetPos())
		else
			sound.Play("cem111333/capaldi1/powerdown.wav", interior:GetPos())
		end
	end
end)

net.Receive("capaldi1-SetVortex", function()
	local capaldi1=net.ReadEntity()
	local on=tobool(net.ReadBit())
	if IsValid(capaldi1) then
		capaldi1.invortex=on
	end
end)

surface.CreateFont( "HUDNumber", {font="Trebuchet MS", size=40, weight=900} )

hook.Add("HUDPaint", "capaldi1-DrawHUD", function()
	local p = LocalPlayer()
	local capaldi1 = p.capaldi1
	if capaldi1 and IsValid(capaldi1) and capaldi1.health and (tobool(GetConVarNumber("capaldi1_takedamage"))==true or capaldi1.exploded) then
		local health = math.floor(capaldi1.health)
		local n=0
		if health <= 99 then
			n=20
		end
		if health <= 9 then
			n=40
		end
		local col=Color(255,255,255)
		if health <= 20 then
			col=Color(255,0,0)
		end
		draw.RoundedBox( 0, 5, ScrH()-55, 220-n, 50, Color(0, 0, 0, 180) )
		draw.DrawText("Health: "..health.."%","HUDNumber", 15, ScrH()-52, col)
	end
end)

hook.Add("CalcView", "capaldi1_CLView", function( ply, origin, angles, fov )
	local capaldi1=LocalPlayer().capaldi1
	local viewent = LocalPlayer():GetViewEntity()
	if !IsValid(viewent) then viewent = LocalPlayer() end
	local dist= -300
	
	if capaldi1 and IsValid(capaldi1) and viewent==LocalPlayer() and not LocalPlayer().capaldi1_viewmode then
		local pos=capaldi1:GetPos()+(capaldi1:GetUp()*50)
		local tracedata={}
		tracedata.start=pos
		tracedata.endpos=pos+ply:GetAimVector():GetNormal()*dist
		tracedata.mask=MASK_NPCWORLDSTATIC
		local trace=util.TraceLine(tracedata)
		local view = {}
		view.origin = trace.HitPos
		view.angles = angles
		return view
	end
end)

hook.Add( "HUDShouldDraw", "capaldi1-HideHUD", function(name)
	local viewmode=LocalPlayer().capaldi1_viewmode
	if ((name == "CHudHealth") or (name == "CHudBattery")) and viewmode then
		return false
	end
end)