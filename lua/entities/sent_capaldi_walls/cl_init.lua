include('shared.lua')

function ENT:Draw()
	if LocalPlayer().capaldi==self:GetNWEntity("capaldi", NULL) and LocalPlayer().capaldi_viewmode and not LocalPlayer().capaldi_render then
		self:DrawModel()
	end
end

function ENT:Think()
	local capaldi=self:GetNWEntity("capaldi",NULL)
	if IsValid(capaldi) and LocalPlayer().capaldi_viewmode and LocalPlayer().capaldi==capaldi then
		if not capaldi.power or capaldi.repairing then
			self:SetMaterial("models/vtalanov98old/toyota/wallssmith")
		else
			self:SetMaterial("models/vtalanov98old/toyota/wallscapaldi")
		end
        end
end