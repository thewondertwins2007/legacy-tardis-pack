AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

util.AddNetworkString("capaldi1Int-Gramophone-GUI")
util.AddNetworkString("capaldi1Int-Gramophone-Bounce")
util.AddNetworkString("capaldi1Int-Gramophone-Send")

function ENT:Initialize()
	self:SetModel( "models/cem111333/capaldi1/audiosystemcap.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_NORMAL )
	self:SetColor(Color(165,165,165,255))
	self.phys = self:GetPhysicsObject()
	self:SetNWEntity("capaldi1",self.capaldi1)
	self.usecur=0
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
end

function ENT:Use( activator, caller, type, value )

	if ( !activator:IsPlayer() ) then return end		-- Who the frig is pressing this shit!?
	if IsValid(self.capaldi1) and ((self.capaldi1.isomorphic and not (activator==self.owner)) or not self.capaldi1.power) then
		return
	end
	
	local interior=self.interior
	local capaldi1=self.capaldi1
	if IsValid(self) and IsValid(interior) and IsValid(capaldi1) then
		interior.usecur=CurTime()+1
		if CurTime()>self.usecur then
			self.usecur=CurTime()+1
			net.Start("capaldi1Int-Gramophone-GUI")
				net.WriteEntity(self)
				net.WriteEntity(capaldi1)
				net.WriteEntity(interior)
			net.Send(activator)
		end
	end
	
	self:NextThink( CurTime() )
	return true
end

net.Receive("capaldi1Int-Gramophone-Bounce", function(l,ply)
	local gramophone=net.ReadEntity()
	local capaldi1=net.ReadEntity()
	local interior=net.ReadEntity()
	local play=tobool(net.ReadBit())
	local choice=net.ReadFloat()
	local custom=tobool(net.ReadBit())
	local customstr
	if custom then
		customstr=net.ReadString()
	end
	if IsValid(gramophone) and IsValid(capaldi1) and IsValid(interior) then
		net.Start("capaldi1Int-Gramophone-Send")
			net.WriteEntity(gramophone)
			net.WriteEntity(capaldi1)
			net.WriteEntity(interior)
			net.WriteBit(play)
			if play then
				net.WriteFloat(choice)
			end
			if custom then
				net.WriteBit(true)
				net.WriteString(customstr)
			else
				net.WriteBit(false)
			end
		net.Send(capaldi1.occupants)
	end
end)