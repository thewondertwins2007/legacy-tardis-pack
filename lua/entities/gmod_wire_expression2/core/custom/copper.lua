/******************************************************************************\
	copper custom E2 functions by Dr. Matt
\******************************************************************************/

E2Lib.RegisterExtension("copper", true)

local function CheckPP(ply, ent) // Prop Protection
	return hook.Call("PhysgunPickup", GAMEMODE, ply, ent)
end

local function copper_Get(ent)
	if ent and IsValid(ent) then
		if ent:GetClass()=="sent_copper_interior" and IsValid(ent.copper) then
			return ent.copper
		elseif ent:GetClass()=="sent_copper" then
			return ent
		elseif ent:IsPlayer() then
			if IsValid(ent.copper) then
				return ent.copper
			else
				return NULL
			end
		else
			return NULL
		end
	end
	return NULL
end

local function copper_Teleport(data,ent,pos,ang)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local pos=Vector(pos[1], pos[2], pos[3])
		if ang then ang=Angle(ang[1], ang[2], ang[3]) end
		local success=ent:Go(pos,ang)
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function copper_Phase(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local success=ent:TogglePhase()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Flightmode(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local success=ent:ToggleFlight()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Lock(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local success=ent:ToggleLocked()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Physlock(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local success=ent:TogglePhysLock()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Power(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local success=ent:TogglePower()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Isomorph(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") or not IsValid(data.player) then return 0 end
		local success=ent:IsomorphicToggle(data.player)
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Longflight(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local success=ent:ToggleLongFlight()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Materialise(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local success=ent:LongReappear()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Selfrepair(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local success=ent:ToggleRepair()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Track(data,ent,trackent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local success=ent:SetTrackingEnt(trackent)
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Spinmode(data,ent,spinmode)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		ent:SetSpinMode(spinmode)
		return ent.spinmode
	end
	return 0
end

local function copper_SetDestination(data,ent,pos,ang)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local pos=Vector(pos[1], pos[2], pos[3])
		if ang then ang=Angle(ang[1], ang[2], ang[3]) end
		if ent.invortex then
			ent:SetDestination(pos,ang)
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function copper_FastReturn(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local success=ent:FastReturn()
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function copper_HADS(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local success=ent:ToggleHADS()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_FastDemat(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		local success=ent:DematFast()
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

// get details

local function copper_Moving(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.moving then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Visible(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.visible then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Flying(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.flightmode then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Locked(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.locked then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Physlocked(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.physlocked then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Powered(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.power then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Isomorphic(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.isomorphic then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Longflighted(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.longflight then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Selfrepairing(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.repairing then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_LastPos(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.lastpos then
			return ent.lastpos
		else
			return Vector()
		end
	end
	return Vector()
end

local function copper_LastAng(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.lastang then
			return {ent.lastang.p, ent.lastang.y, ent.lastang.r}
		else
			return {0,0,0}
		end
	end
	return {0,0,0}
end

local function copper_Health(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.health then
			return math.floor(ent.health)
		else
			return 0
		end
	end
	return 0
end

local function copper_Tracking(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if IsValid(ent.trackingent) then
			return ent.trackingent
		else
			return NULL
		end
	end
	return NULL
end

local function copper_InVortex(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.invortex then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_IsHADS(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return 0 end
		if ent.hads then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function copper_Pilot(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_copper") then return NULL end
		if not ent.pilot then return NULL end
		return ent.pilot
	end
	return NULL
end

--------------------------------------------------------------------------------

//set details
e2function entity entity:copperGet()
	return copper_Get(this)
end

e2function number entity:copperDemat(vector pos)
	return copper_Teleport(self, this, pos)
end

e2function number entity:copperDemat(vector pos, angle rot)
	return copper_Teleport(self, this, pos, rot)
end

e2function number entity:copperPhase()
	return copper_Phase(self, this)
end

e2function number entity:copperFlightmode()
	return copper_Flightmode(self, this)
end

e2function number entity:copperLock()
	return copper_Lock(self, this)
end

e2function number entity:copperPhyslock()
	return copper_Physlock(self, this)
end

e2function number entity:copperPower()
	return copper_Power(self, this)
end

e2function number entity:copperIsomorph()
	return copper_Isomorph(self,this)
end

e2function number entity:copperLongflight()
	return copper_Longflight(self, this)
end

e2function number entity:copperMaterialise()
	return copper_Materialise(self, this)
end

e2function number entity:copperSelfrepair()
	return copper_Selfrepair(self, this)
end

e2function number entity:copperTrack(entity ent)
	return copper_Track(self, this, ent)
end

e2function number entity:copperSpinmode(number spinmode)
	return copper_Spinmode(self, this, spinmode)
end

e2function number entity:copperSetDestination(vector pos)
	return copper_SetDestination(self, this, pos)
end

e2function number entity:copperSetDestination(vector pos, angle rot)
	return copper_SetDestination(self, this, pos, rot)
end

e2function number entity:copperFastReturn()
	return copper_FastReturn(self, this)
end

e2function number entity:copperHADS()
	return copper_HADS(self, this)
end

e2function number entity:copperFastDemat()
	return copper_FastDemat(self, this)
end

// get details
e2function number entity:copperMoving()
	return copper_Moving(this)
end

e2function number entity:copperVisible()
	return copper_Visible(this)
end

e2function number entity:copperFlying()
	return copper_Flying(this)
end

e2function number entity:copperHealth()
	return copper_Health(this)
end

e2function number entity:copperLocked()
	return copper_Locked(this)
end

e2function number entity:copperPhyslocked()
	return copper_Physlocked(this)
end

e2function number entity:copperPowered()
	return copper_Powered(this)
end

e2function number entity:copperIsomorphic()
	return copper_Isomorphic(this)
end

e2function number entity:copperLongflighted()
	return copper_Longflighted(this)
end

e2function number entity:copperSelfrepairing()
	return copper_Selfrepairing(this)
end

e2function vector entity:copperLastPos()
	return copper_LastPos(this)
end

e2function angle entity:copperLastAng()
	return copper_LastAng(this)
end

e2function entity entity:copperTracking()
	return copper_Tracking(this)
end

e2function number entity:copperInVortex()
	return copper_InVortex(this)
end

e2function number entity:copperIsHADS()
	return copper_IsHADS(this)
end

e2function entity entity:copperPilot()
	return copper_Pilot(this)
end