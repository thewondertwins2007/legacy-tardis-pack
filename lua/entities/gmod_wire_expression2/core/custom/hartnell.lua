/******************************************************************************\
	hartnell custom E2 functions by Dr. Matt
\******************************************************************************/

E2Lib.RegisterExtension("hartnell", true)

local function CheckPP(ply, ent) // Prop Protection
	return hook.Call("PhysgunPickup", GAMEMODE, ply, ent)
end

local function hartnell_Get(ent)
	if ent and IsValid(ent) then
		if ent:GetClass()=="sent_hartnell_interior" and IsValid(ent.hartnell) then
			return ent.hartnell
		elseif ent:GetClass()=="sent_hartnell" then
			return ent
		elseif ent:IsPlayer() then
			if IsValid(ent.hartnell) then
				return ent.hartnell
			else
				return NULL
			end
		else
			return NULL
		end
	end
	return NULL
end

local function hartnell_Teleport(data,ent,pos,ang)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local pos=Vector(pos[1], pos[2], pos[3])
		if ang then ang=Angle(ang[1], ang[2], ang[3]) end
		local success=ent:Go(pos,ang)
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function hartnell_Phase(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local success=ent:TogglePhase()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Flightmode(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local success=ent:ToggleFlight()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Lock(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local success=ent:ToggleLocked()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Physlock(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local success=ent:TogglePhysLock()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Power(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local success=ent:TogglePower()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Isomorph(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") or not IsValid(data.player) then return 0 end
		local success=ent:IsomorphicToggle(data.player)
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Longflight(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local success=ent:ToggleLongFlight()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Materialise(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local success=ent:LongReappear()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Selfrepair(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local success=ent:ToggleRepair()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Track(data,ent,trackent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local success=ent:SetTrackingEnt(trackent)
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Spinmode(data,ent,spinmode)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		ent:SetSpinMode(spinmode)
		return ent.spinmode
	end
	return 0
end

local function hartnell_SetDestination(data,ent,pos,ang)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local pos=Vector(pos[1], pos[2], pos[3])
		if ang then ang=Angle(ang[1], ang[2], ang[3]) end
		if ent.invortex then
			ent:SetDestination(pos,ang)
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function hartnell_FastReturn(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local success=ent:FastReturn()
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function hartnell_HADS(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local success=ent:ToggleHADS()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_FastDemat(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		local success=ent:DematFast()
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

// get details

local function hartnell_Moving(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.moving then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Visible(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.visible then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Flying(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.flightmode then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Locked(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.locked then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Physlocked(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.physlocked then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Powered(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.power then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Isomorphic(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.isomorphic then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Longflighted(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.longflight then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Selfrepairing(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.repairing then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_LastPos(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.lastpos then
			return ent.lastpos
		else
			return Vector()
		end
	end
	return Vector()
end

local function hartnell_LastAng(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.lastang then
			return {ent.lastang.p, ent.lastang.y, ent.lastang.r}
		else
			return {0,0,0}
		end
	end
	return {0,0,0}
end

local function hartnell_Health(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.health then
			return math.floor(ent.health)
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Tracking(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if IsValid(ent.trackingent) then
			return ent.trackingent
		else
			return NULL
		end
	end
	return NULL
end

local function hartnell_InVortex(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.invortex then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_IsHADS(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return 0 end
		if ent.hads then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function hartnell_Pilot(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_hartnell") then return NULL end
		if not ent.pilot then return NULL end
		return ent.pilot
	end
	return NULL
end

--------------------------------------------------------------------------------

//set details
e2function entity entity:hartnellGet()
	return hartnell_Get(this)
end

e2function number entity:hartnellDemat(vector pos)
	return hartnell_Teleport(self, this, pos)
end

e2function number entity:hartnellDemat(vector pos, angle rot)
	return hartnell_Teleport(self, this, pos, rot)
end

e2function number entity:hartnellPhase()
	return hartnell_Phase(self, this)
end

e2function number entity:hartnellFlightmode()
	return hartnell_Flightmode(self, this)
end

e2function number entity:hartnellLock()
	return hartnell_Lock(self, this)
end

e2function number entity:hartnellPhyslock()
	return hartnell_Physlock(self, this)
end

e2function number entity:hartnellPower()
	return hartnell_Power(self, this)
end

e2function number entity:hartnellIsomorph()
	return hartnell_Isomorph(self,this)
end

e2function number entity:hartnellLongflight()
	return hartnell_Longflight(self, this)
end

e2function number entity:hartnellMaterialise()
	return hartnell_Materialise(self, this)
end

e2function number entity:hartnellSelfrepair()
	return hartnell_Selfrepair(self, this)
end

e2function number entity:hartnellTrack(entity ent)
	return hartnell_Track(self, this, ent)
end

e2function number entity:hartnellSpinmode(number spinmode)
	return hartnell_Spinmode(self, this, spinmode)
end

e2function number entity:hartnellSetDestination(vector pos)
	return hartnell_SetDestination(self, this, pos)
end

e2function number entity:hartnellSetDestination(vector pos, angle rot)
	return hartnell_SetDestination(self, this, pos, rot)
end

e2function number entity:hartnellFastReturn()
	return hartnell_FastReturn(self, this)
end

e2function number entity:hartnellHADS()
	return hartnell_HADS(self, this)
end

e2function number entity:hartnellFastDemat()
	return hartnell_FastDemat(self, this)
end

// get details
e2function number entity:hartnellMoving()
	return hartnell_Moving(this)
end

e2function number entity:hartnellVisible()
	return hartnell_Visible(this)
end

e2function number entity:hartnellFlying()
	return hartnell_Flying(this)
end

e2function number entity:hartnellHealth()
	return hartnell_Health(this)
end

e2function number entity:hartnellLocked()
	return hartnell_Locked(this)
end

e2function number entity:hartnellPhyslocked()
	return hartnell_Physlocked(this)
end

e2function number entity:hartnellPowered()
	return hartnell_Powered(this)
end

e2function number entity:hartnellIsomorphic()
	return hartnell_Isomorphic(this)
end

e2function number entity:hartnellLongflighted()
	return hartnell_Longflighted(this)
end

e2function number entity:hartnellSelfrepairing()
	return hartnell_Selfrepairing(this)
end

e2function vector entity:hartnellLastPos()
	return hartnell_LastPos(this)
end

e2function angle entity:hartnellLastAng()
	return hartnell_LastAng(this)
end

e2function entity entity:hartnellTracking()
	return hartnell_Tracking(this)
end

e2function number entity:hartnellInVortex()
	return hartnell_InVortex(this)
end

e2function number entity:hartnellIsHADS()
	return hartnell_IsHADS(this)
end

e2function entity entity:hartnellPilot()
	return hartnell_Pilot(this)
end