/******************************************************************************\
	capaldiold custom E2 functions by Dr. Matt
\******************************************************************************/

E2Lib.RegisterExtension("capaldiold", true)

local function CheckPP(ply, ent) // Prop Protection
	return hook.Call("PhysgunPickup", GAMEMODE, ply, ent)
end

local function capaldiold_Get(ent)
	if ent and IsValid(ent) then
		if ent:GetClass()=="sent_capaldiold_interior" and IsValid(ent.capaldiold) then
			return ent.capaldiold
		elseif ent:GetClass()=="sent_capaldiold" then
			return ent
		elseif ent:IsPlayer() then
			if IsValid(ent.capaldiold) then
				return ent.capaldiold
			else
				return NULL
			end
		else
			return NULL
		end
	end
	return NULL
end

local function capaldiold_Teleport(data,ent,pos,ang)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local pos=Vector(pos[1], pos[2], pos[3])
		if ang then ang=Angle(ang[1], ang[2], ang[3]) end
		local success=ent:Go(pos,ang)
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function capaldiold_Phase(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local success=ent:TogglePhase()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Flightmode(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local success=ent:ToggleFlight()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Lock(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local success=ent:ToggleLocked()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Physlock(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local success=ent:TogglePhysLock()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Power(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local success=ent:TogglePower()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Isomorph(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") or not IsValid(data.player) then return 0 end
		local success=ent:IsomorphicToggle(data.player)
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Longflight(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local success=ent:ToggleLongFlight()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Materialise(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local success=ent:LongReappear()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Selfrepair(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local success=ent:ToggleRepair()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Track(data,ent,trackent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local success=ent:SetTrackingEnt(trackent)
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Spinmode(data,ent,spinmode)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		ent:SetSpinMode(spinmode)
		return ent.spinmode
	end
	return 0
end

local function capaldiold_SetDestination(data,ent,pos,ang)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local pos=Vector(pos[1], pos[2], pos[3])
		if ang then ang=Angle(ang[1], ang[2], ang[3]) end
		if ent.invortex then
			ent:SetDestination(pos,ang)
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function capaldiold_FastReturn(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local success=ent:FastReturn()
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function capaldiold_HADS(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local success=ent:ToggleHADS()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_FastDemat(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		local success=ent:DematFast()
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

// get details

local function capaldiold_Moving(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.moving then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Visible(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.visible then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Flying(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.flightmode then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Locked(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.locked then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Physlocked(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.physlocked then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Powered(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.power then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Isomorphic(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.isomorphic then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Longflighted(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.longflight then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Selfrepairing(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.repairing then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_LastPos(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.lastpos then
			return ent.lastpos
		else
			return Vector()
		end
	end
	return Vector()
end

local function capaldiold_LastAng(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.lastang then
			return {ent.lastang.p, ent.lastang.y, ent.lastang.r}
		else
			return {0,0,0}
		end
	end
	return {0,0,0}
end

local function capaldiold_Health(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.health then
			return math.floor(ent.health)
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Tracking(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if IsValid(ent.trackingent) then
			return ent.trackingent
		else
			return NULL
		end
	end
	return NULL
end

local function capaldiold_InVortex(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.invortex then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_IsHADS(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return 0 end
		if ent.hads then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function capaldiold_Pilot(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_capaldiold") then return NULL end
		if not ent.pilot then return NULL end
		return ent.pilot
	end
	return NULL
end

--------------------------------------------------------------------------------

//set details
e2function entity entity:capaldioldGet()
	return capaldiold_Get(this)
end

e2function number entity:capaldioldDemat(vector pos)
	return capaldiold_Teleport(self, this, pos)
end

e2function number entity:capaldioldDemat(vector pos, angle rot)
	return capaldiold_Teleport(self, this, pos, rot)
end

e2function number entity:capaldioldPhase()
	return capaldiold_Phase(self, this)
end

e2function number entity:capaldioldFlightmode()
	return capaldiold_Flightmode(self, this)
end

e2function number entity:capaldioldLock()
	return capaldiold_Lock(self, this)
end

e2function number entity:capaldioldPhyslock()
	return capaldiold_Physlock(self, this)
end

e2function number entity:capaldioldPower()
	return capaldiold_Power(self, this)
end

e2function number entity:capaldioldIsomorph()
	return capaldiold_Isomorph(self,this)
end

e2function number entity:capaldioldLongflight()
	return capaldiold_Longflight(self, this)
end

e2function number entity:capaldioldMaterialise()
	return capaldiold_Materialise(self, this)
end

e2function number entity:capaldioldSelfrepair()
	return capaldiold_Selfrepair(self, this)
end

e2function number entity:capaldioldTrack(entity ent)
	return capaldiold_Track(self, this, ent)
end

e2function number entity:capaldioldSpinmode(number spinmode)
	return capaldiold_Spinmode(self, this, spinmode)
end

e2function number entity:capaldioldSetDestination(vector pos)
	return capaldiold_SetDestination(self, this, pos)
end

e2function number entity:capaldioldSetDestination(vector pos, angle rot)
	return capaldiold_SetDestination(self, this, pos, rot)
end

e2function number entity:capaldioldFastReturn()
	return capaldiold_FastReturn(self, this)
end

e2function number entity:capaldioldHADS()
	return capaldiold_HADS(self, this)
end

e2function number entity:capaldioldFastDemat()
	return capaldiold_FastDemat(self, this)
end

// get details
e2function number entity:capaldioldMoving()
	return capaldiold_Moving(this)
end

e2function number entity:capaldioldVisible()
	return capaldiold_Visible(this)
end

e2function number entity:capaldioldFlying()
	return capaldiold_Flying(this)
end

e2function number entity:capaldioldHealth()
	return capaldiold_Health(this)
end

e2function number entity:capaldioldLocked()
	return capaldiold_Locked(this)
end

e2function number entity:capaldioldPhyslocked()
	return capaldiold_Physlocked(this)
end

e2function number entity:capaldioldPowered()
	return capaldiold_Powered(this)
end

e2function number entity:capaldioldIsomorphic()
	return capaldiold_Isomorphic(this)
end

e2function number entity:capaldioldLongflighted()
	return capaldiold_Longflighted(this)
end

e2function number entity:capaldioldSelfrepairing()
	return capaldiold_Selfrepairing(this)
end

e2function vector entity:capaldioldLastPos()
	return capaldiold_LastPos(this)
end

e2function angle entity:capaldioldLastAng()
	return capaldiold_LastAng(this)
end

e2function entity entity:capaldioldTracking()
	return capaldiold_Tracking(this)
end

e2function number entity:capaldioldInVortex()
	return capaldiold_InVortex(this)
end

e2function number entity:capaldioldIsHADS()
	return capaldiold_IsHADS(this)
end

e2function entity entity:capaldioldPilot()
	return capaldiold_Pilot(this)
end