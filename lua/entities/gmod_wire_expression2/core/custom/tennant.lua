/******************************************************************************\
	tennant custom E2 functions by Dr. Matt
\******************************************************************************/

E2Lib.RegisterExtension("tennant", true)

local function CheckPP(ply, ent) // Prop Protection
	return hook.Call("PhysgunPickup", GAMEMODE, ply, ent)
end

local function tennant_Get(ent)
	if ent and IsValid(ent) then
		if ent:GetClass()=="sent_tennant_interior" and IsValid(ent.tennant) then
			return ent.tennant
		elseif ent:GetClass()=="sent_tennant" then
			return ent
		elseif ent:IsPlayer() then
			if IsValid(ent.tennant) then
				return ent.tennant
			else
				return NULL
			end
		else
			return NULL
		end
	end
	return NULL
end

local function tennant_Teleport(data,ent,pos,ang)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local pos=Vector(pos[1], pos[2], pos[3])
		if ang then ang=Angle(ang[1], ang[2], ang[3]) end
		local success=ent:Go(pos,ang)
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function tennant_Phase(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local success=ent:TogglePhase()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Flightmode(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local success=ent:ToggleFlight()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Lock(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local success=ent:ToggleLocked()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Physlock(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local success=ent:TogglePhysLock()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Power(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local success=ent:TogglePower()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Isomorph(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") or not IsValid(data.player) then return 0 end
		local success=ent:IsomorphicToggle(data.player)
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Longflight(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local success=ent:ToggleLongFlight()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Materialise(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local success=ent:LongReappear()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Selfrepair(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local success=ent:ToggleRepair()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Track(data,ent,trackent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local success=ent:SetTrackingEnt(trackent)
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Spinmode(data,ent,spinmode)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		ent:SetSpinMode(spinmode)
		return ent.spinmode
	end
	return 0
end

local function tennant_SetDestination(data,ent,pos,ang)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local pos=Vector(pos[1], pos[2], pos[3])
		if ang then ang=Angle(ang[1], ang[2], ang[3]) end
		if ent.invortex then
			ent:SetDestination(pos,ang)
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function tennant_FastReturn(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local success=ent:FastReturn()
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function tennant_HADS(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local success=ent:ToggleHADS()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_FastDemat(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		local success=ent:DematFast()
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

// get details

local function tennant_Moving(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.moving then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Visible(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.visible then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Flying(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.flightmode then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Locked(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.locked then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Physlocked(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.physlocked then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Powered(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.power then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Isomorphic(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.isomorphic then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Longflighted(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.longflight then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Selfrepairing(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.repairing then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_LastPos(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.lastpos then
			return ent.lastpos
		else
			return Vector()
		end
	end
	return Vector()
end

local function tennant_LastAng(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.lastang then
			return {ent.lastang.p, ent.lastang.y, ent.lastang.r}
		else
			return {0,0,0}
		end
	end
	return {0,0,0}
end

local function tennant_Health(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.health then
			return math.floor(ent.health)
		else
			return 0
		end
	end
	return 0
end

local function tennant_Tracking(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if IsValid(ent.trackingent) then
			return ent.trackingent
		else
			return NULL
		end
	end
	return NULL
end

local function tennant_InVortex(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.invortex then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_IsHADS(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return 0 end
		if ent.hads then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function tennant_Pilot(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_tennant") then return NULL end
		if not ent.pilot then return NULL end
		return ent.pilot
	end
	return NULL
end

--------------------------------------------------------------------------------

//set details
e2function entity entity:tennantGet()
	return tennant_Get(this)
end

e2function number entity:tennantDemat(vector pos)
	return tennant_Teleport(self, this, pos)
end

e2function number entity:tennantDemat(vector pos, angle rot)
	return tennant_Teleport(self, this, pos, rot)
end

e2function number entity:tennantPhase()
	return tennant_Phase(self, this)
end

e2function number entity:tennantFlightmode()
	return tennant_Flightmode(self, this)
end

e2function number entity:tennantLock()
	return tennant_Lock(self, this)
end

e2function number entity:tennantPhyslock()
	return tennant_Physlock(self, this)
end

e2function number entity:tennantPower()
	return tennant_Power(self, this)
end

e2function number entity:tennantIsomorph()
	return tennant_Isomorph(self,this)
end

e2function number entity:tennantLongflight()
	return tennant_Longflight(self, this)
end

e2function number entity:tennantMaterialise()
	return tennant_Materialise(self, this)
end

e2function number entity:tennantSelfrepair()
	return tennant_Selfrepair(self, this)
end

e2function number entity:tennantTrack(entity ent)
	return tennant_Track(self, this, ent)
end

e2function number entity:tennantSpinmode(number spinmode)
	return tennant_Spinmode(self, this, spinmode)
end

e2function number entity:tennantSetDestination(vector pos)
	return tennant_SetDestination(self, this, pos)
end

e2function number entity:tennantSetDestination(vector pos, angle rot)
	return tennant_SetDestination(self, this, pos, rot)
end

e2function number entity:tennantFastReturn()
	return tennant_FastReturn(self, this)
end

e2function number entity:tennantHADS()
	return tennant_HADS(self, this)
end

e2function number entity:tennantFastDemat()
	return tennant_FastDemat(self, this)
end

// get details
e2function number entity:tennantMoving()
	return tennant_Moving(this)
end

e2function number entity:tennantVisible()
	return tennant_Visible(this)
end

e2function number entity:tennantFlying()
	return tennant_Flying(this)
end

e2function number entity:tennantHealth()
	return tennant_Health(this)
end

e2function number entity:tennantLocked()
	return tennant_Locked(this)
end

e2function number entity:tennantPhyslocked()
	return tennant_Physlocked(this)
end

e2function number entity:tennantPowered()
	return tennant_Powered(this)
end

e2function number entity:tennantIsomorphic()
	return tennant_Isomorphic(this)
end

e2function number entity:tennantLongflighted()
	return tennant_Longflighted(this)
end

e2function number entity:tennantSelfrepairing()
	return tennant_Selfrepairing(this)
end

e2function vector entity:tennantLastPos()
	return tennant_LastPos(this)
end

e2function angle entity:tennantLastAng()
	return tennant_LastAng(this)
end

e2function entity entity:tennantTracking()
	return tennant_Tracking(this)
end

e2function number entity:tennantInVortex()
	return tennant_InVortex(this)
end

e2function number entity:tennantIsHADS()
	return tennant_IsHADS(this)
end

e2function entity entity:tennantPilot()
	return tennant_Pilot(this)
end