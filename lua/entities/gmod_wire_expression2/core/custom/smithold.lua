/******************************************************************************\
	smithold custom E2 functions by Dr. Matt
\******************************************************************************/

E2Lib.RegisterExtension("smithold", true)

local function CheckPP(ply, ent) // Prop Protection
	return hook.Call("PhysgunPickup", GAMEMODE, ply, ent)
end

local function smithold_Get(ent)
	if ent and IsValid(ent) then
		if ent:GetClass()=="sent_smithold_interior" and IsValid(ent.smithold) then
			return ent.smithold
		elseif ent:GetClass()=="sent_smithold" then
			return ent
		elseif ent:IsPlayer() then
			if IsValid(ent.smithold) then
				return ent.smithold
			else
				return NULL
			end
		else
			return NULL
		end
	end
	return NULL
end

local function smithold_Teleport(data,ent,pos,ang)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local pos=Vector(pos[1], pos[2], pos[3])
		if ang then ang=Angle(ang[1], ang[2], ang[3]) end
		local success=ent:Go(pos,ang)
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function smithold_Phase(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local success=ent:TogglePhase()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Flightmode(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local success=ent:ToggleFlight()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Lock(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local success=ent:ToggleLocked()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Physlock(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local success=ent:TogglePhysLock()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Power(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local success=ent:TogglePower()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Isomorph(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") or not IsValid(data.player) then return 0 end
		local success=ent:IsomorphicToggle(data.player)
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Longflight(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local success=ent:ToggleLongFlight()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Materialise(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local success=ent:LongReappear()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Selfrepair(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local success=ent:ToggleRepair()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Track(data,ent,trackent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local success=ent:SetTrackingEnt(trackent)
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Spinmode(data,ent,spinmode)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		ent:SetSpinMode(spinmode)
		return ent.spinmode
	end
	return 0
end

local function smithold_SetDestination(data,ent,pos,ang)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local pos=Vector(pos[1], pos[2], pos[3])
		if ang then ang=Angle(ang[1], ang[2], ang[3]) end
		if ent.invortex then
			ent:SetDestination(pos,ang)
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function smithold_FastReturn(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local success=ent:FastReturn()
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

local function smithold_HADS(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local success=ent:ToggleHADS()
		if success then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_FastDemat(data,ent)
	if ent and IsValid(ent) and CheckPP(data.player,ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		local success=ent:DematFast()
		if success then
			return 1
		else
			return 0
		end
	else
		return 0
	end
end

// get details

local function smithold_Moving(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.moving then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Visible(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.visible then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Flying(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.flightmode then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Locked(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.locked then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Physlocked(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.physlocked then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Powered(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.power then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Isomorphic(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.isomorphic then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Longflighted(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.longflight then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Selfrepairing(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.repairing then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_LastPos(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.lastpos then
			return ent.lastpos
		else
			return Vector()
		end
	end
	return Vector()
end

local function smithold_LastAng(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.lastang then
			return {ent.lastang.p, ent.lastang.y, ent.lastang.r}
		else
			return {0,0,0}
		end
	end
	return {0,0,0}
end

local function smithold_Health(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.health then
			return math.floor(ent.health)
		else
			return 0
		end
	end
	return 0
end

local function smithold_Tracking(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if IsValid(ent.trackingent) then
			return ent.trackingent
		else
			return NULL
		end
	end
	return NULL
end

local function smithold_InVortex(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.invortex then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_IsHADS(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return 0 end
		if ent.hads then
			return 1
		else
			return 0
		end
	end
	return 0
end

local function smithold_Pilot(ent)
	if ent and IsValid(ent) then
		if not (ent:GetClass()=="sent_smithold") then return NULL end
		if not ent.pilot then return NULL end
		return ent.pilot
	end
	return NULL
end

--------------------------------------------------------------------------------

//set details
e2function entity entity:smitholdGet()
	return smithold_Get(this)
end

e2function number entity:smitholdDemat(vector pos)
	return smithold_Teleport(self, this, pos)
end

e2function number entity:smitholdDemat(vector pos, angle rot)
	return smithold_Teleport(self, this, pos, rot)
end

e2function number entity:smitholdPhase()
	return smithold_Phase(self, this)
end

e2function number entity:smitholdFlightmode()
	return smithold_Flightmode(self, this)
end

e2function number entity:smitholdLock()
	return smithold_Lock(self, this)
end

e2function number entity:smitholdPhyslock()
	return smithold_Physlock(self, this)
end

e2function number entity:smitholdPower()
	return smithold_Power(self, this)
end

e2function number entity:smitholdIsomorph()
	return smithold_Isomorph(self,this)
end

e2function number entity:smitholdLongflight()
	return smithold_Longflight(self, this)
end

e2function number entity:smitholdMaterialise()
	return smithold_Materialise(self, this)
end

e2function number entity:smitholdSelfrepair()
	return smithold_Selfrepair(self, this)
end

e2function number entity:smitholdTrack(entity ent)
	return smithold_Track(self, this, ent)
end

e2function number entity:smitholdSpinmode(number spinmode)
	return smithold_Spinmode(self, this, spinmode)
end

e2function number entity:smitholdSetDestination(vector pos)
	return smithold_SetDestination(self, this, pos)
end

e2function number entity:smitholdSetDestination(vector pos, angle rot)
	return smithold_SetDestination(self, this, pos, rot)
end

e2function number entity:smitholdFastReturn()
	return smithold_FastReturn(self, this)
end

e2function number entity:smitholdHADS()
	return smithold_HADS(self, this)
end

e2function number entity:smitholdFastDemat()
	return smithold_FastDemat(self, this)
end

// get details
e2function number entity:smitholdMoving()
	return smithold_Moving(this)
end

e2function number entity:smitholdVisible()
	return smithold_Visible(this)
end

e2function number entity:smitholdFlying()
	return smithold_Flying(this)
end

e2function number entity:smitholdHealth()
	return smithold_Health(this)
end

e2function number entity:smitholdLocked()
	return smithold_Locked(this)
end

e2function number entity:smitholdPhyslocked()
	return smithold_Physlocked(this)
end

e2function number entity:smitholdPowered()
	return smithold_Powered(this)
end

e2function number entity:smitholdIsomorphic()
	return smithold_Isomorphic(this)
end

e2function number entity:smitholdLongflighted()
	return smithold_Longflighted(this)
end

e2function number entity:smitholdSelfrepairing()
	return smithold_Selfrepairing(this)
end

e2function vector entity:smitholdLastPos()
	return smithold_LastPos(this)
end

e2function angle entity:smitholdLastAng()
	return smithold_LastAng(this)
end

e2function entity entity:smitholdTracking()
	return smithold_Tracking(this)
end

e2function number entity:smitholdInVortex()
	return smithold_InVortex(this)
end

e2function number entity:smitholdIsHADS()
	return smithold_IsHADS(this)
end

e2function entity entity:smitholdPilot()
	return smithold_Pilot(this)
end