AddCSLuaFile( "von.lua" )
AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

util.AddNetworkString("hartnellInt-Locations-GUI")
util.AddNetworkString("hartnellInt-Locations-Send")

function ENT:Initialize()
	self:SetModel( "models/vtalanov98old/hartnell/coordinate.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_NORMAL )
	self:SetColor(Color(255,255,255,255))
	self.phys = self:GetPhysicsObject()
	self:SetNWEntity("hartnell",self.hartnell)
	self.usecur=0
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
end

function ENT:Use( activator, caller, type, value )

	if ( !activator:IsPlayer() ) then return end		-- Who the frig is pressing this shit!?
	if IsValid(self.hartnell) and ((self.hartnell.isomorphic and not (activator==self.owner)) or not self.hartnell.power) then
		return
	end
	
	local interior=self.interior
	local hartnell=self.hartnell
	if IsValid(interior) and IsValid(self.hartnell) and IsValid(self) then
		interior.usecur=CurTime()+1
		if CurTime()>self.usecur then
			self.usecur=CurTime()+1
			if tobool(GetConVarNumber("hartnell_advanced"))==true then
				interior:UpdateAdv(activator,false)
			end
			net.Start("hartnellInt-Locations-GUI")
				net.WriteEntity(self.interior)
				net.WriteEntity(self.hartnell)
				net.WriteEntity(self)
			net.Send(activator)
		end
	end

	self:NextThink( CurTime() )
	return true
end

net.Receive("hartnellInt-Locations-Send", function(l,ply)
	local interior=net.ReadEntity()
	local hartnell=net.ReadEntity()
	local typewriter=net.ReadEntity()
	if IsValid(interior) and IsValid(hartnell) and IsValid(typewriter) then
		local pos=Vector(net.ReadFloat(), net.ReadFloat(), net.ReadFloat())
		local ang=Angle(net.ReadFloat(), net.ReadFloat(), net.ReadFloat())
		if tobool(GetConVarNumber("hartnell_advanced"))==true then
			if interior.flightmode==0 and interior.step==0 then
				local success=interior:StartAdv(2,ply,pos,ang)
				if success then
					ply:ChatPrint("Coordinate flightmode activated.")
				end
			else
				interior:UpdateAdv(ply,false)
			end
		else
			if not hartnell.invortex then
				typewriter.pos=pos
				typewriter.ang=ang
				ply:ChatPrint("TARDIS destination set.")
			end
		end
		
		if hartnell.invortex then
			hartnell:SetDestination(pos,ang)
			ply:ChatPrint("TARDIS destination set.")
		end
	end
end)
