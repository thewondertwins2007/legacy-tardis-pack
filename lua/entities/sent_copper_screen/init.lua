AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

function ENT:Initialize()
	self:SetModel( "models/vtalanov98old/copper/screen.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_NORMAL )
	self:SetColor(Color(255,255,255,255))
	self.phys = self:GetPhysicsObject()
	self:SetNWEntity("copper",self.copper)
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
end

function ENT:Use( activator, caller, type, value )

	if ( !activator:IsPlayer() ) then return end		-- Who the frig is pressing this shit!?
	
	if IsValid(self.interior.skycamera) and CurTime()>self.interior.skycamera.usecur then
		local skycamera=self.interior.skycamera
		skycamera.usecur=CurTime()+1
		skycamera:PlayerEnter(activator)
	end
	if IsValid(self.copper) then
		net.Start("copperInt-ControlSound")
			net.WriteEntity(self.copper)
			net.WriteEntity(self)
			net.WriteString("vtalanov98old/copper/screen.wav")
		net.Broadcast()
	end
	
	self:NextThink( CurTime() )
	return true
end