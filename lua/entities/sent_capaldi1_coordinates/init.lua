AddCSLuaFile( "von.lua" )
AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

util.AddNetworkString("capaldi1Int-Locations-GUI")
util.AddNetworkString("capaldi1Int-Locations-Send")

function ENT:Initialize()
	self:SetModel( "models/cem111333/capaldi1/keyboard.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_NORMAL )
	self:SetColor(Color(165,165,165,255))
	self.phys = self:GetPhysicsObject()
	self:SetNWEntity("capaldi1",self.capaldi1)
	self.usecur=0
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
end

function ENT:Use( activator, caller, type, value )

	if ( !activator:IsPlayer() ) then return end		-- Who the frig is pressing this shit!?
	if IsValid(self.capaldi1) and ((self.capaldi1.isomorphic and not (activator==self.owner)) or not self.capaldi1.power) then
		return
	end
	
	local interior=self.interior
	local capaldi1=self.capaldi1
	if IsValid(interior) and IsValid(self.capaldi1) and IsValid(self) then
		interior.usecur=CurTime()+1
		if CurTime()>self.usecur then
			self.usecur=CurTime()+1
			if tobool(GetConVarNumber("capaldi1_advanced"))==true then
				interior:UpdateAdv(activator,false)
			end
			net.Start("capaldi1Int-Locations-GUI")
				net.WriteEntity(self.interior)
				net.WriteEntity(self.capaldi1)
				net.WriteEntity(self)
			net.Send(activator)
			if IsValid(self.capaldi1) then
				net.Start("capaldi1Int-ControlSound")
					net.WriteEntity(self.capaldi1)
					net.WriteEntity(self)
					net.WriteString("cem111333/capaldi1/keyboard.wav")
				net.Broadcast()
			end
		end
	end
	
	self:NextThink( CurTime() )
	return true
end

net.Receive("capaldi1Int-Locations-Send", function(l,ply)
	local interior=net.ReadEntity()
	local capaldi1=net.ReadEntity()
	local typewriter=net.ReadEntity()
	if IsValid(interior) and IsValid(capaldi1) and IsValid(typewriter) then
		local pos=Vector(net.ReadFloat(), net.ReadFloat(), net.ReadFloat())
		local ang=Angle(net.ReadFloat(), net.ReadFloat(), net.ReadFloat())
		if tobool(GetConVarNumber("capaldi1_advanced"))==true then
			if interior.flightmode==0 and interior.step==0 then
				local success=interior:StartAdv(2,ply,pos,ang)
				if success then
					ply:ChatPrint("Programmable flightmode activated.")
				end
			else
				interior:UpdateAdv(ply,false)
			end
		else
			if not capaldi1.invortex then
				typewriter.pos=pos
				typewriter.ang=ang
				ply:ChatPrint("TARDIS destination set.")
			end
		end
		
		if capaldi1.invortex then
			capaldi1:SetDestination(pos,ang)
			ply:ChatPrint("TARDIS destination set.")
		end
	end
end)