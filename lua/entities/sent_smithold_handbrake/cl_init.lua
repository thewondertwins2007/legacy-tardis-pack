include('shared.lua')

function ENT:Draw()
	if LocalPlayer().smithold==self:GetNWEntity("smithold", NULL) and LocalPlayer().smithold_viewmode and not LocalPlayer().smithold_render then
		self:DrawModel()
	end
end

function ENT:Initialize()
	self.PosePosition = 0.5
end

function ENT:Think()
	local smithold=self:GetNWEntity("smithold",NULL)
	if LocalPlayer().smithold==self:GetNWEntity("smithold", NULL) and LocalPlayer().smithold_viewmode then
		local TargetPos = 0.0;
		if ( self:GetOn() ) then TargetPos = 1.0; end
		self.PosePosition = math.Approach( self.PosePosition, TargetPos, FrameTime() * 3 )
		self:SetPoseParameter( "switch", self.PosePosition )
		self:InvalidateBoneCache()
		
	end
end