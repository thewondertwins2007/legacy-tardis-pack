include('shared.lua')
 
--[[---------------------------------------------------------
   Name: Draw
   Purpose: Draw the model in-game.
   Remember, the things you render first will be underneath!
---------------------------------------------------------]]
function ENT:Draw() 
	if not self.phasing and self.visible and not self.invortex then
		self:DrawModel()
		if WireLib then
			Wire_Render(self)
		end
	elseif self.phasing then
		if self.percent then
			if not self.phasemode and self.highPer <= 0 then
				self.phasing=false
			elseif self.phasemode and self.percent >= 1 then
				self.phasing=false
			end
		end
		
		self.percent = (self.phaselifetime - CurTime())
		self.highPer = self.percent + 0.5
		if self.phasemode then
			self.percent = (1-self.percent)-0.5
			self.highPer = self.percent+0.5
		end
		self.percent = math.Clamp( self.percent, 0, 1 )
		self.highPer = math.Clamp( self.highPer, 0, 1 )

		--Drawing original model
		local normal = self:GetUp()
		local origin = self:GetPos() + self:GetUp() * (self.maxs.z - ( self.height * self.highPer ))
		local distance = normal:Dot( origin )
		
		render.EnableClipping( true )
		render.PushCustomClipPlane( normal, distance )
			self:DrawModel()
		render.PopCustomClipPlane()
		
		local restoreT = self:GetMaterial()
		
		--Drawing phase texture
		render.MaterialOverride( self.wiremat )

		normal = self:GetUp()
		distance = normal:Dot( origin )
		render.PushCustomClipPlane( normal, distance )
		
		local normal2 = self:GetUp() * -1
		local origin2 = self:GetPos() + self:GetUp() * (self.maxs.z - ( self.height * self.percent ))
		local distance2 = normal2:Dot( origin2 )
		render.PushCustomClipPlane( normal2, distance2 )
			self:DrawModel()
		render.PopCustomClipPlane()
		render.PopCustomClipPlane()
		
		render.MaterialOverride( restoreT )
		render.EnableClipping( false )
	end
end

function ENT:Phase(mode)
	self.phasing=true
	self.phaseactive=true
	self.phaselifetime=CurTime()+1
	self.phasemode=mode
end

function ENT:Initialize()
	self.health=100
	self.phasemode=false
	self.visible=true
	self.flightmode=false
	self.visible=true
	self.power=true
	self.z=0
	self.phasedraw=0
	self.mins = self:OBBMins()
	self.maxs = self:OBBMaxs()
	self.wiremat = Material( "models/vtalanov98old/capaldi/phase" )
	self.height = self.maxs.z - self.mins.z
end

function ENT:OnRemove()
	if self.flightloop then
		self.flightloop:Stop()
		self.flightloop=nil
	end
	if self.flightloop2 then
		self.flightloop2:Stop()
		self.flightloop2=nil
	end
end

function ENT:Think()
	if tobool(GetConVarNumber("capaldiold_flightsound"))==true then
		if not self.flightloop then
			self.flightloop=CreateSound(self, "vtalanov98old/capaldi/flight_loop.wav")
			self.flightloop:SetSoundLevel(90)
			self.flightloop:Stop()
		end
		if self.flightmode and self.visible and not self.moving then
			if !self.flightloop:IsPlaying() then
				self.flightloop:Play()
			end
			local e = LocalPlayer():GetViewEntity()
			if !IsValid(e) then e = LocalPlayer() end
			local capaldiold=LocalPlayer().capaldiold
			if not (capaldiold and IsValid(capaldiold) and capaldiold==self and e==LocalPlayer()) then
				local pos = e:GetPos()
				local spos = self:GetPos()
				local doppler = (pos:Distance(spos+e:GetVelocity())-pos:Distance(spos+self:GetVelocity()))/200
				if self.exploded then
					local r=math.random(90,130)
					self.flightloop:ChangePitch(math.Clamp(r+doppler,80,120),0.1)
				else
					self.flightloop:ChangePitch(math.Clamp((self:GetVelocity():Length()/250)+95+doppler,80,120),0.1)
				end
				self.flightloop:ChangeVolume(GetConVarNumber("capaldiold_flightvol"),0)
			else
				if self.exploded then
					local r=math.random(90,130)
					self.flightloop:ChangePitch(r,0.1)
				else
					local p=math.Clamp(self:GetVelocity():Length()/250,0,15)
					self.flightloop:ChangePitch(95+p,0.1)
				end
				self.flightloop:ChangeVolume(0.75*GetConVarNumber("capaldiold_flightvol"),0)
			end
		else
			if self.flightloop:IsPlaying() then
				self.flightloop:Stop()
			end
		end
		
		local interior=self:GetNWEntity("interior",NULL)
		if not self.flightloop2 and interior and IsValid(interior) then
			self.flightloop2=CreateSound(interior, "vtalanov98old/capaldi/flight_loop_int.wav")
			self.flightloop2:Stop()
		end
		if self.flightloop2 and (self.flightmode or self.invortex) and LocalPlayer().capaldiold_viewmode and not IsValid(LocalPlayer().capaldiold_skycamera) and interior and IsValid(interior) and ((self.invortex and self.moving) or not self.moving) then
			if !self.flightloop2:IsPlaying() then
				self.flightloop2:Play()
				self.flightloop2:ChangeVolume(0.4,0)
			end
			if self.exploded then
				local r=math.random(90,130)
				self.flightloop2:ChangePitch(r,0.1)
			else
				local p=math.Clamp(self:GetVelocity():Length()/250,0,15)
				self.flightloop2:ChangePitch(95+p,0.1)
			end
		elseif self.flightloop2 then
			if self.flightloop2:IsPlaying() then
				self.flightloop2:Stop()
			end
		end
	else
		if self.flightloop then
			self.flightloop:Stop()
			self.flightloop=nil
		end
		if self.flightloop2 then
			self.flightloop2:Stop()
			self.flightloop2=nil
		end
	end
	
	if self.light_on and tobool(GetConVarNumber("capaldiold_dynamiclight"))==true then
		local dlight = DynamicLight( self:EntIndex() )
		if ( dlight ) then
			local size=400
			local v=self:GetNWVector("extcol",Vector(255,255,255))
			local c=Color(v.x,v.y,v.z)
			dlight.Pos = self:GetPos() + self:GetUp() * 123
			dlight.r = c.r
			dlight.g = c.g
			dlight.b = c.b
			dlight.Brightness = 1
			dlight.Decay = size * 5
			dlight.Size = size
			dlight.DieTime = CurTime() + 1
		end
	end
	if self.health and self.health > 20 and self.visible and self.power and not self.moving and tobool(GetConVarNumber("capaldiold_dynamiclight"))==true then
		local dlight2 = DynamicLight( self:EntIndex() )
		if ( dlight2 ) then
			local size=400
			local v=self:GetNWVector("extcol",Vector(255,255,255))
			local c=Color(v.x,v.y,v.z)
			dlight2.Pos = self:GetPos() + self:GetUp() * 123
			dlight2.r = c.r
			dlight2.g = c.g
			dlight2.b = c.b
			dlight2.Brightness = 1
			dlight2.Decay = size * 5
			dlight2.Size = size
			dlight2.DieTime = CurTime() + 1
		end
	end
end

net.Receive("capaldiold-UpdateVis", function()
	local ent=net.ReadEntity()
	ent.visible=tobool(net.ReadBit())
end)

net.Receive("capaldiold-Phase", function()
	local capaldiold=net.ReadEntity()
	local interior=net.ReadEntity()
	if IsValid(capaldiold) then
		capaldiold.visible=tobool(net.ReadBit())
		capaldiold:Phase(capaldiold.visible)
		if not capaldiold.visible and tobool(GetConVarNumber("capaldiold_phasesound"))==true then
			capaldiold:EmitSound("vtalanov98old/capaldi/phase_enable.wav", 100, 100)
			if IsValid(interior) then
				interior:EmitSound("vtalanov98old/capaldi/phase_enable.wav", 100, 100)
			end
		end
	end
end)

net.Receive("capaldiold-Explode", function()
	local ent=net.ReadEntity()
	ent.exploded=true
end)

net.Receive("capaldiold-UnExplode", function()
	local ent=net.ReadEntity()
	ent.exploded=false
end)

net.Receive("capaldiold-Flightmode", function()
	local ent=net.ReadEntity()
	ent.flightmode=tobool(net.ReadBit())
end)

net.Receive("capaldiold-SetInterior", function()
	local ent=net.ReadEntity()
	ent.interior=net.ReadEntity()
end)

local tpsounds={}
tpsounds[0]={ // normal
	"vtalanov98old/capaldi/demat.wav",
	"vtalanov98old/capaldi/mat.wav",
	"vtalanov98old/capaldi/full.wav"
}
tpsounds[1]={ // fast demat
	"vtalanov98old/capaldi/fast_demat.wav",
	"vtalanov98old/capaldi/mat.wav",
	"vtalanov98old/capaldi/full.wav"
}
tpsounds[2]={ // fast return
	"vtalanov98old/capaldi/fastreturn_demat_int.wav",
	"vtalanov98old/capaldi/fastreturn_mat_int.wav",
	"vtalanov98old/capaldi/fastreturn_full_int.wav"
}
net.Receive("capaldiold-Go", function()
	local capaldiold=net.ReadEntity()
	if IsValid(capaldiold) then
		capaldiold.moving=true
	end
	local interior=net.ReadEntity()
	local exploded=tobool(net.ReadBit())
	local pitch=(exploded and 110 or 100)
	local long=tobool(net.ReadBit())
	local snd=net.ReadFloat()
	local snds=tpsounds[snd]
	if tobool(GetConVarNumber("capaldiold_matsound"))==true then
		if IsValid(capaldiold) and LocalPlayer().capaldiold==capaldiold then
			if capaldiold.visible then
				if long then
					capaldiold:EmitSound(snds[1], 100, pitch)
				else
					capaldiold:EmitSound(snds[3], 100, pitch)
				end
			end
			if interior and IsValid(interior) and LocalPlayer().capaldiold_viewmode and not IsValid(LocalPlayer().capaldiold_skycamera) then
				if long then
				sound.Play("vtalanov98old/capaldi/demat_int.wav", interior:LocalToWorld(Vector(0,0,90)))
				else
				sound.Play("vtalanov98old/capaldi/full_int.wav", interior:LocalToWorld(Vector(0,0,90)))
				end
			end
		elseif IsValid(capaldiold) and capaldiold.visible then
			local pos=net.ReadVector()
			local pos2=net.ReadVector()
			if pos then
				sound.Play(snds[1], pos, 75, pitch)
			end
			if pos2 and not long then
				sound.Play("vtalanov98old/capaldi/mat.wav", pos2, 75, pitch)
			end
		end
	end
end)

net.Receive("capaldiold-Stop", function()
	capaldiold=net.ReadEntity()
	if IsValid(capaldiold) then
		capaldiold.moving=nil
	end
end)

net.Receive("capaldiold-Reappear", function()
	local capaldiold=net.ReadEntity()
	local interior=net.ReadEntity()
	local exploded=tobool(net.ReadBit())
	local pitch=(exploded and 110 or 100)
	local snd=net.ReadFloat()
	local snds=tpsounds[snd]
	if tobool(GetConVarNumber("capaldiold_matsound"))==true then
		if IsValid(capaldiold) and LocalPlayer().capaldiold==capaldiold then
			if capaldiold.visible then
				capaldiold:EmitSound("vtalanov98old/capaldi/mat.wav", 100, pitch)
			end
			if interior and IsValid(interior) and LocalPlayer().capaldiold_viewmode and not IsValid(LocalPlayer().capaldiold_skycamera) then
				sound.Play("vtalanov98old/capaldi/mat_int.wav", interior:LocalToWorld(Vector(0,0,90)))
			end
		elseif IsValid(capaldiold) and capaldiold.visible then
			sound.Play("vtalanov98old/capaldi/mat.wav", net.ReadVector(), 75, pitch)
		end
	end
end)

net.Receive("Player-Setcapaldiold", function()
	local ply=net.ReadEntity()
	ply.capaldiold=net.ReadEntity()
end)

net.Receive("capaldiold-SetHealth", function()
	local capaldiold=net.ReadEntity()
	capaldiold.health=net.ReadFloat()
end)

net.Receive("capaldiold-SetLocked", function()
	local capaldiold=net.ReadEntity()
	local interior=net.ReadEntity()
	local locked=tobool(net.ReadBit())
	local makesound=tobool(net.ReadBit())
	if IsValid(capaldiold) then
		capaldiold.locked=locked
		if tobool(GetConVarNumber("capaldiold_locksound"))==true and makesound then
			sound.Play("vtalanov98old/capaldi/lock.wav", capaldiold:GetPos())
		end
	end
	if IsValid(interior) then
		if tobool(GetConVarNumber("capaldiold_locksound"))==true and not IsValid(LocalPlayer().capaldiold_skycamera) and makesound then
			sound.Play("vtalanov98old/capaldi/lock.wav", interior:LocalToWorld(Vector(-270,0,89)))
		end
	end
end)

net.Receive("capaldiold-SetViewmode", function()
	LocalPlayer().capaldiold_viewmode=tobool(net.ReadBit())
	LocalPlayer().ShouldDisableLegs=(not LocalPlayer().capaldiold_viewmode)
	
	if LocalPlayer().capaldiold_viewmode and GetConVarNumber("r_rootlod")>0 then
		Derma_Query("The TARDIS Interior requires model detail on high, set now?", "TARDIS Interior", "Yes", function() RunConsoleCommand("r_rootlod", 0) end, "No", function() end)
	end
		
end)

hook.Add( "ShouldDrawLocalPlayer", "capaldiold-ShouldDrawLocalPlayer", function(ply)
	if IsValid(ply.capaldiold) and not ply.capaldiold_viewmode then
		return false
	end
end)

net.Receive("capaldiold-PlayerEnter", function()
	if tobool(GetConVarNumber("capaldiold_doorsound"))==true then
		local ent1=net.ReadEntity()
		local ent2=net.ReadEntity()
		if IsValid(ent1) and ent1.visible then
			sound.Play("vtalanov98old/capaldi/door.wav", ent1:GetPos())
		end
		if IsValid(ent2) and not IsValid(LocalPlayer().capaldiold_skycamera) then
			sound.Play("vtalanov98old/capaldi/door.wav", ent2:LocalToWorld(Vector(-270,0,89)))
		end
	end
end)

net.Receive("capaldiold-PlayerExit", function()
	if tobool(GetConVarNumber("capaldiold_doorsound"))==true then
		local ent1=net.ReadEntity()
		local ent2=net.ReadEntity()
		if IsValid(ent1) and ent1.visible then
			sound.Play("vtalanov98old/capaldi/door.wav", ent1:GetPos())
		end
		if IsValid(ent2) and not IsValid(LocalPlayer().capaldiold_skycamera) then
			sound.Play("vtalanov98old/capaldi/door.wav", ent2:LocalToWorld(Vector(-270,0,89)))
		end
	end
end)

net.Receive("capaldiold-SetRepairing", function()
	local capaldiold=net.ReadEntity()
	local repairing=tobool(net.ReadBit())
	local interior=net.ReadEntity()
	if IsValid(capaldiold) then
		capaldiold.repairing=repairing
	end
	if IsValid(interior) and LocalPlayer().capaldiold==capaldiold and LocalPlayer().capaldiold_viewmode and tobool(GetConVarNumber("capaldioldint_powersound"))==true then
		if repairing then
			sound.Play("vtalanov98old/capaldi/powerdown.wav", interior:GetPos())
		else
			sound.Play("vtalanov98old/capaldi/powerup.wav", interior:GetPos())
		end
	end
end)

net.Receive("capaldiold-BeginRepair", function()
	local capaldiold=net.ReadEntity()
	if IsValid(capaldiold) then
		/*
		local mat=Material("models/vtalanov98old/capaldiold/2014")
		if not mat:IsError() then
			mat:SetTexture("$basetexture", "models/props_combine/metal_combinebridge001")
		end
		*/
	end
end)

net.Receive("capaldiold-FinishRepair", function()
	local capaldiold=net.ReadEntity()
	if IsValid(capaldiold) then
		if tobool(GetConVarNumber("capaldioldint_repairsound"))==true and capaldiold.visible then
			sound.Play("vtalanov98old/capaldi/repairfinish.wav", capaldiold:GetPos())
		end
		/*
		local mat=Material("models/vtalanov98old/capaldiold/2010")
		if not mat:IsError() then
			mat:SetTexture("$basetexture", "models/vtalanov98old/capaldiold/2014")
		end
		*/
	end
end)

net.Receive("capaldiold-SetLight", function()
	local capaldiold=net.ReadEntity()
	local on=tobool(net.ReadBit())
	if IsValid(capaldiold) then
		capaldiold.light_on=on
	end
end)

net.Receive("capaldiold-SetPower", function()
	local capaldiold=net.ReadEntity()
	local on=tobool(net.ReadBit())
	local interior=net.ReadEntity()
	if IsValid(capaldiold) then
		capaldiold.power=on
	end
	if IsValid(interior) and LocalPlayer().capaldiold==capaldiold and LocalPlayer().capaldiold_viewmode and tobool(GetConVarNumber("capaldioldint_powersound"))==true then
		if on then
			sound.Play("vtalanov98old/capaldi/powerup.wav", interior:GetPos())
		else
			sound.Play("vtalanov98old/capaldi/powerdown.wav", interior:GetPos())
		end
	end
end)

net.Receive("capaldiold-SetVortex", function()
	local capaldiold=net.ReadEntity()
	local on=tobool(net.ReadBit())
	if IsValid(capaldiold) then
		capaldiold.invortex=on
	end
end)

surface.CreateFont( "HUDNumber", {font="Trebuchet MS", size=40, weight=900} )

hook.Add("HUDPaint", "capaldiold-DrawHUD", function()
	local p = LocalPlayer()
	local capaldiold = p.capaldiold
	if capaldiold and IsValid(capaldiold) and capaldiold.health and (tobool(GetConVarNumber("capaldiold_takedamage"))==true or capaldiold.exploded) then
		local health = math.floor(capaldiold.health)
		local n=0
		if health <= 99 then
			n=20
		end
		if health <= 9 then
			n=40
		end
		local col=Color(255,255,255)
		if health <= 20 then
			col=Color(255,0,0)
		end
		draw.RoundedBox( 0, 5, ScrH()-55, 220-n, 50, Color(0, 0, 0, 180) )
		draw.DrawText("Health: "..health.."%","HUDNumber", 15, ScrH()-52, col)
	end
end)

hook.Add("CalcView", "capaldiold_CLView", function( ply, origin, angles, fov )
	local capaldiold=LocalPlayer().capaldiold
	local viewent = LocalPlayer():GetViewEntity()
	if !IsValid(viewent) then viewent = LocalPlayer() end
	local dist= -300
	
	if capaldiold and IsValid(capaldiold) and viewent==LocalPlayer() and not LocalPlayer().capaldiold_viewmode then
		local pos=capaldiold:GetPos()+(capaldiold:GetUp()*50)
		local tracedata={}
		tracedata.start=pos
		tracedata.endpos=pos+ply:GetAimVector():GetNormal()*dist
		tracedata.mask=MASK_NPCWORLDSTATIC
		local trace=util.TraceLine(tracedata)
		local view = {}
		view.origin = trace.HitPos
		view.angles = angles
		return view
	end
end)

hook.Add( "HUDShouldDraw", "capaldiold-HideHUD", function(name)
	local viewmode=LocalPlayer().capaldiold_viewmode
	if ((name == "CHudHealth") or (name == "CHudBattery")) and viewmode then
		return false
	end
end)