include('shared.lua')

function ENT:Draw()
	if LocalPlayer().capaldi1==self:GetNWEntity("capaldi1", NULL) and LocalPlayer().capaldi1_viewmode and not LocalPlayer().capaldi1_render then
		self:DrawModel()
	end
end

function ENT:Initialize()
	self.PosePosition = 0
end

function ENT:Think()
	if LocalPlayer().capaldi1==self:GetNWEntity("capaldi1", NULL) and LocalPlayer().capaldi1_viewmode then
		local TargetPos = 0.0;
		if ( self:GetOn() ) then TargetPos = 1.0; end
		self.PosePosition = math.Approach( self.PosePosition, TargetPos, FrameTime() * 1 )
		self:SetPoseParameter( "switch", self.PosePosition )
		self:InvalidateBoneCache()
		
	end
end