include('shared.lua')

function ENT:Draw()
	if LocalPlayer().capaldi1==self:GetNWEntity("capaldi1", NULL) and LocalPlayer().capaldi1_viewmode and not LocalPlayer().capaldi1_render then
		self:DrawModel()
	end
end

function ENT:Initialize()
	self.PosePosition = 0
end

function ENT:Think()
	local capaldi1=self:GetNWEntity("capaldi1", NULL)
	if IsValid(capaldi1) and LocalPlayer().capaldi1==capaldi1 and LocalPlayer().capaldi1_viewmode then
		local mode=self:GetMode()
		if (capaldi1.flightmode or capaldi1.moving) and not (mode==0) then
			local TargetPos
			if ( mode==-1 ) then
				TargetPos = 1.0
				if self.PosePosition==1 then
					self.PosePosition=0
				end
			elseif ( mode==1 ) then
				TargetPos = 0.0
				if self.PosePosition==0 then
					self.PosePosition=1
				end
			end
			self.PosePosition = math.Approach( self.PosePosition, TargetPos, FrameTime() * 1 )
			self:SetPoseParameter( "switch", self.PosePosition )
			self:InvalidateBoneCache()
		end
		
	end
end