AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

function ENT:Initialize()
	self:SetModel( "models/cem111333/capaldi1/consolecap.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_NORMAL )
	self:SetUseType( ONOFF_USE )
	self:SetColor(Color(165,165,165,255))
	self.phys = self:GetPhysicsObject()
	self:SetNWEntity("capaldi1",self.capaldi1)
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
	self.viewcur=0
	self.usecur=0
end

function ENT:Use( ply )
	if CurTime()>self.usecur and self.capaldi1 and IsValid(self.capaldi1) and ply.capaldi1 and IsValid(ply.capaldi1) and ply.capaldi1==self.capaldi1 and ply.capaldi1_viewmode and not ply.capaldi1_skycamera then
		if CurTime()>self.capaldi1.viewmodecur then
			self.capaldi1:ToggleViewmode(ply)
			self.usecur=CurTime()+1
			self.capaldi1.viewmodecur=CurTime()+1
		end
	end
end