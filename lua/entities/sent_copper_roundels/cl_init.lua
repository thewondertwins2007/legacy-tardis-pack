include('shared.lua')

function ENT:Draw()
	if LocalPlayer().copper==self:GetNWEntity("copper", NULL) and LocalPlayer().copper_viewmode and not LocalPlayer().copper_render then
		self:DrawModel()
	end
end

function ENT:Initialize()
	self.catwalklights={}
	self.catwalklights.pos=0
	self.catwalklights.mode=1
	self.PosePosition = 0.5
end

function ENT:Think()
	local copper=self:GetNWEntity("copper",NULL)
	if IsValid(copper) and LocalPlayer().copper_viewmode and LocalPlayer().copper==copper then
	  if LocalPlayer().copper==self:GetNWEntity("copper", NULL) and LocalPlayer().copper_viewmode then
		  local TargetPos = 0.0;
		  if ( self:GetOn() ) then TargetPos = 1.0; end
		  self.PosePosition = math.Approach( self.PosePosition, TargetPos, FrameTime() * 1.5 )
		  self:SetPoseParameter( "catwalklights", self.PosePosition )
		  self:InvalidateBoneCache()
		
	  end
			if copper.health and copper.health > 0 and not copper.repairing and copper.power then
	                          self:SetColor(Color(255,255,255))
                        else
                                  self:SetColor(Color(0,0,0))
			end
                        mat=Material("models/vtalanov98old/copper/roundels")

			if not copper.moving and not copper.flightmode then
			          self:SetMaterial("models/vtalanov98old/copper/roundelsstatic")
                        else
                                  self:SetMaterial("models/vtalanov98old/copper/roundels")
			end
        end
end