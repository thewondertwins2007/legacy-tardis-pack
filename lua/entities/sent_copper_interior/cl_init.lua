include('shared.lua')
 
--[[---------------------------------------------------------
   Name: Draw
   Purpose: Draw the model in-game.
   Remember, the things you render first will be underneath!
---------------------------------------------------------]]
function ENT:Draw()
	if LocalPlayer().copper_viewmode and self:GetNWEntity("copper",NULL)==LocalPlayer().copper and not LocalPlayer().copper_render then
		self:DrawModel()
		if WireLib then
			Wire_Render(self)
		end
	end
end

function ENT:OnRemove()
	if self.cloisterbell then
		self.cloisterbell:Stop()
		self.cloisterbell=nil
	end
	if self.creaks then
		self.creaks:Stop()
		self.creaks=nil
	end
	if self.idlesound then
		self.idlesound:Stop()
		self.idlesound=nil
	end
end

function ENT:Initialize()
	self.timerotor={}
	self.timerotor.pos=0
	self.timerotor.mode=1
	self.parts={}
end

net.Receive("copperInt-SetParts", function()
	local t={}
	local interior=net.ReadEntity()
	local count=net.ReadFloat()
	for i=1,count do
		local ent=net.ReadEntity()
		ent.copper_part=true
		if IsValid(interior) then
			table.insert(interior.parts,ent)
		end
	end
end)

net.Receive("copperInt-UpdateAdv", function()
	local success=tobool(net.ReadBit())
	if success then
	else
		surface.PlaySound("vtalanov98old/copper/cloister.wav")
	end
end)

net.Receive("copperInt-SetAdv", function()
	local interior=net.ReadEntity()
	local ply=net.ReadEntity()
	local mode=net.ReadFloat()
	if IsValid(interior) and IsValid(ply) and mode then
		if ply==LocalPlayer() then
		end
		interior.flightmode=mode
	end
end)

net.Receive("copperInt-ControlSound", function()
	local copper=net.ReadEntity()
	local control=net.ReadEntity()
	local snd=net.ReadString()
	if IsValid(copper) and IsValid(control) and snd and tobool(GetConVarNumber("copperint_controlsound"))==true and LocalPlayer().copper==copper and LocalPlayer().copper_viewmode then
		sound.Play(snd,control:GetPos())
	end
end)

function ENT:Think()
	local copper=self:GetNWEntity("copper",NULL)
	if IsValid(copper) and LocalPlayer().copper_viewmode and LocalPlayer().copper==copper then
		if tobool(GetConVarNumber("copperint_cloisterbell"))==true and not IsValid(LocalPlayer().copper_skycamera) then
			if copper.health and copper.health < 21 then
				if self.cloisterbell and !self.cloisterbell:IsPlaying() then
					self.cloisterbell:Play()
				elseif not self.cloisterbell then
					self.cloisterbell = CreateSound(self, "vtalanov98old/copper/cloisterbell_loop.wav")
					self.cloisterbell:Play()
				end
			else
				if self.cloisterbell and self.cloisterbell:IsPlaying() then
					self.cloisterbell:Stop()
					self.cloisterbell=nil
				end
			end
		else
			if self.cloisterbell and self.cloisterbell:IsPlaying() then
				self.cloisterbell:Stop()
				self.cloisterbell=nil
			end
		end

		if tobool(GetConVarNumber("copperint_creaks"))==true and not IsValid(LocalPlayer().copper_skycamera) then
			if not copper.power or copper.repairing then
				if self.creaks and !self.creaks:IsPlaying() then
					self.creaks:Play()
				elseif not self.creaks then
					self.creaks = CreateSound(self, "vtalanov98old/copper/creaks_loop.wav")
					self.creaks:Play()
				        self.creaks:ChangeVolume(0.4,0)
				end
			else
				if self.creaks and self.creaks:IsPlaying() then
					self.creaks:Stop()
					self.creaks=nil
				end
			end
		else
			if self.creaks and self.creaks:IsPlaying() then
				self.creaks:Stop()
				self.creaks=nil
			end
		end
		
		if tobool(GetConVarNumber("copperint_idlesound"))==true and copper.health and copper.health >= 1 and not IsValid(LocalPlayer().copper_skycamera) and not copper.repairing and copper.power then
			if self.idlesound and !self.idlesound:IsPlaying() then
				self.idlesound:Play()
			elseif not self.idlesound then
				self.idlesound = CreateSound(self, "vtalanov98old/copper/interior_idle_loop.wav")
				self.idlesound:Play()
				self.idlesound:ChangeVolume(1,0)
			end
		else
			if self.idlesound and self.idlesound:IsPlaying() then
				self.idlesound:Stop()
				self.idlesound=nil
			end
		end
		
		if not IsValid(LocalPlayer().copper_skycamera) and tobool(GetConVarNumber("copperint_dynamiclight"))==true then
			if copper.health and copper.health > 21 and not copper.repairing and copper.power then
				local dlight = DynamicLight( self:EntIndex() )
				if ( dlight ) then
					local size=1024
					local v=self:GetNWVector("mainlight",Vector(255,50,0))
					dlight.Pos = self:LocalToWorld(Vector(0,0,120))
					dlight.r = v.x
					dlight.g = v.y
					dlight.b = v.z
					dlight.Brightness = 5
					dlight.Decay = size * 5
					dlight.Size = size
					dlight.DieTime = CurTime() + 1
				end
			end

			if copper.health and copper.health > 0 and not copper.repairing and copper.power then			
			   local dlight2 = DynamicLight( self:EntIndex()+10000 )
			   if ( dlight2 ) then
				   local size=1024
				   local v=self:GetNWVector("seclight",Vector(0,255,0))
					if copper.health < 21 then
						v=self:GetNWVector("warnlight",Vector(255,0,0))
					end
				   dlight2.Pos = self:LocalToWorld(Vector(0,0,-50))
				   dlight2.r = v.x
				   dlight2.g = v.y
				   dlight2.b = v.z
				   dlight2.Brightness = 3
				   dlight2.Decay = size * 5
				   dlight2.Size = size
				   dlight2.DieTime = CurTime() + 1
			      end
			   end
			
			if (self.timerotor.pos>0 and not copper.moving or copper.flightmode) or (copper.moving or copper.flightmode) then
				if self.timerotor.pos==1 then
					self.timerotor.mode=0
				elseif self.timerotor.pos==0 and (copper.moving or copper.flightmode) then
					self.timerotor.mode=1
				end
				
				self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.55 )
				self:SetPoseParameter( "glass", self.timerotor.pos )
			end
		end
	else
		if self.cloisterbell then
			self.cloisterbell:Stop()
			self.cloisterbell=nil
		end
		if self.creaks then
			self.creaks:Stop()
			self.creaks=nil
		end
		if self.idlesound then
			self.idlesound:Stop()
			self.idlesound=nil
		end
	end
end