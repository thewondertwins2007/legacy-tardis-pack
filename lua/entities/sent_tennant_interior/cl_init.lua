include('shared.lua')
 
--[[---------------------------------------------------------
   Name: Draw
   Purpose: Draw the model in-game.
   Remember, the things you render first will be underneath!
---------------------------------------------------------]]
function ENT:Draw()
	if LocalPlayer().tennant_viewmode and self:GetNWEntity("tennant",NULL)==LocalPlayer().tennant and not LocalPlayer().tennant_render then
		self:DrawModel()
		if WireLib then
			Wire_Render(self)
		end
	end
end

function ENT:OnRemove()
	if self.cloisterbell then
		self.cloisterbell:Stop()
		self.cloisterbell=nil
	end
	if self.creaks then
		self.creaks:Stop()
		self.creaks=nil
	end
	if self.idlesound then
		self.idlesound:Stop()
		self.idlesound=nil
	end
end

function ENT:Initialize()
	self.timerotor={}
	self.timerotor.pos=0
	self.timerotor.mode=1
	self.parts={}
end

net.Receive("tennantInt-SetParts", function()
	local t={}
	local interior=net.ReadEntity()
	local count=net.ReadFloat()
	for i=1,count do
		local ent=net.ReadEntity()
		ent.tennant_part=true
		if IsValid(interior) then
			table.insert(interior.parts,ent)
		end
	end
end)

net.Receive("tennantInt-UpdateAdv", function()
	local success=tobool(net.ReadBit())
	if success then
	else
		surface.PlaySound("vtalanov98old/tennant/cloister.wav")
	end
end)

net.Receive("tennantInt-SetAdv", function()
	local interior=net.ReadEntity()
	local ply=net.ReadEntity()
	local mode=net.ReadFloat()
	if IsValid(interior) and IsValid(ply) and mode then
		if ply==LocalPlayer() then
		end
		interior.flightmode=mode
	end
end)

net.Receive("tennantInt-ControlSound", function()
	local tennant=net.ReadEntity()
	local control=net.ReadEntity()
	local snd=net.ReadString()
	if IsValid(tennant) and IsValid(control) and snd and tobool(GetConVarNumber("tennantint_controlsound"))==true and LocalPlayer().tennant==tennant and LocalPlayer().tennant_viewmode then
		sound.Play(snd,control:GetPos())
	end
end)

function ENT:Think()
	local tennant=self:GetNWEntity("tennant",NULL)
	if IsValid(tennant) and LocalPlayer().tennant_viewmode and LocalPlayer().tennant==tennant then
		if tobool(GetConVarNumber("tennantint_cloisterbell"))==true and not IsValid(LocalPlayer().tennant_skycamera) then
			if tennant.health and tennant.health < 21 then
				if self.cloisterbell and !self.cloisterbell:IsPlaying() then
					self.cloisterbell:Play()
				elseif not self.cloisterbell then
					self.cloisterbell = CreateSound(self, "vtalanov98old/tennant/cloisterbell_loop.wav")
					self.cloisterbell:Play()
				end
			else
				if self.cloisterbell and self.cloisterbell:IsPlaying() then
					self.cloisterbell:Stop()
					self.cloisterbell=nil
				end
			end
		else
			if self.cloisterbell and self.cloisterbell:IsPlaying() then
				self.cloisterbell:Stop()
				self.cloisterbell=nil
			end
		end

		if tobool(GetConVarNumber("tennantint_creaks"))==true and not IsValid(LocalPlayer().tennant_skycamera) then
			if not tennant.power or tennant.repairing then
				if self.creaks and !self.creaks:IsPlaying() then
					self.creaks:Play()
				elseif not self.creaks then
					self.creaks = CreateSound(self, "vtalanov98old/tennant/creaks_loop.wav")
					self.creaks:Play()
				        self.creaks:ChangeVolume(0.4,0)
				end
			else
				if self.creaks and self.creaks:IsPlaying() then
					self.creaks:Stop()
					self.creaks=nil
				end
			end
		else
			if self.creaks and self.creaks:IsPlaying() then
				self.creaks:Stop()
				self.creaks=nil
			end
		end
		
		if tobool(GetConVarNumber("tennantint_idlesound"))==true and tennant.health and tennant.health >= 1 and not IsValid(LocalPlayer().tennant_skycamera) and not tennant.repairing and tennant.power then
			if self.idlesound and !self.idlesound:IsPlaying() then
				self.idlesound:Play()
			elseif not self.idlesound then
				self.idlesound = CreateSound(self, "vtalanov98old/tennant/interior_idle_loop.wav")
				self.idlesound:Play()
				self.idlesound:ChangeVolume(0.7,0)
			end
		else
			if self.idlesound and self.idlesound:IsPlaying() then
				self.idlesound:Stop()
				self.idlesound=nil
			end
		end
		
		if not IsValid(LocalPlayer().tennant_skycamera) and tobool(GetConVarNumber("tennantint_dynamiclight"))==true then
			if tennant.health and tennant.health > 21 and not tennant.repairing and tennant.power then
				local dlight = DynamicLight( self:EntIndex() )
				if ( dlight ) then
					local size=512
					local v=self:GetNWVector("mainlight",Vector(0,0,300))
					dlight.Pos = self:LocalToWorld(Vector(0,0,300))
					dlight.r = v.x
					dlight.g = v.y
					dlight.b = v.z
					dlight.Brightness = 5
					dlight.Decay = size * 5
					dlight.Size = size
					dlight.DieTime = CurTime() + 1
				end
			end
			
			if tennant.health and tennant.health > 0 and not tennant.repairing and tennant.power then	
			   local dlight2 = DynamicLight( self:EntIndex()+10000 )
			   if ( dlight2 ) then
				   local size=1024
				   local v=self:GetNWVector("seclight",Vector(0,0, 40))
					if tennant.health < 21 then
						v=self:GetNWVector("warnlight",Vector(0,0,40))
					end
				   dlight2.Pos = self:LocalToWorld(Vector(0,0,40))
				   dlight2.r = v.x
				   dlight2.g = v.y
				   dlight2.b = v.z
				   dlight2.Brightness = 4
				   dlight2.Decay = size * 5
				   dlight2.Size = size
				   dlight2.DieTime = CurTime() + 1
			      end
			end
			
			if (self.timerotor.pos>0 and not tennant.moving or tennant.flightmode) or (tennant.moving or tennant.flightmode) then
				if self.timerotor.pos==1 then
					self.timerotor.mode=0
				elseif self.timerotor.pos==0 and (tennant.moving or tennant.flightmode) then
					self.timerotor.mode=1
				end
				
				self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.5 )
				self:SetPoseParameter( "glass", self.timerotor.pos )
			end
		end
	else
		if self.cloisterbell then
			self.cloisterbell:Stop()
			self.cloisterbell=nil
		end
		if self.creaks then
			self.creaks:Stop()
			self.creaks=nil
		end
		if self.idlesound then
			self.idlesound:Stop()
			self.idlesound=nil
		end
	end
end