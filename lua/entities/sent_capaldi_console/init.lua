AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')

function ENT:Initialize()
	self:SetModel( "models/vtalanov98old/toyota/consolecapaldi.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetRenderMode( RENDERMODE_NORMAL )
	self:SetUseType( ONOFF_USE )
	self:SetColor(Color(100,100,100,255))
	self.phys = self:GetPhysicsObject()
	self:SetNWEntity("capaldi",self.capaldi)
	if (self.phys:IsValid()) then
		self.phys:EnableMotion(false)
	end
	self.viewcur=0
	self.usecur=0
end

function ENT:Use( ply )
	if CurTime()>self.usecur and self.capaldi and IsValid(self.capaldi) and ply.capaldi and IsValid(ply.capaldi) and ply.capaldi==self.capaldi and ply.capaldi_viewmode and not ply.capaldi_skycamera then
		if CurTime()>self.capaldi.viewmodecur then
			self.capaldi:ToggleViewmode(ply)
			self.usecur=CurTime()+1
			self.capaldi.viewmodecur=CurTime()+1
		end
	end
end