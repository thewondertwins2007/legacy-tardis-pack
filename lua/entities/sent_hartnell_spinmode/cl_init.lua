include('shared.lua')

function ENT:Draw()
	if LocalPlayer().hartnell==self:GetNWEntity("hartnell", NULL) and LocalPlayer().hartnell_viewmode and not LocalPlayer().hartnell_render then
		self:DrawModel()
	end
end

function ENT:Initialize()
	self.PosePosition = 0.5
end

function ENT:Think()
	local hartnell=self:GetNWEntity("hartnell", NULL)
	if IsValid(hartnell) and LocalPlayer().hartnell==hartnell and LocalPlayer().hartnell_viewmode then
		local mode=self:GetMode()
		if (hartnell.flightmode or hartnell.moving) and not (mode==0) then
			local TargetPos
			if ( mode==-1 ) then
				TargetPos = 1.0
				if self.PosePosition==1 then
					self.PosePosition=0
				end
			elseif ( mode==1 ) then
				TargetPos = 0.0
				if self.PosePosition==0 then
					self.PosePosition=1
				end
			end
			self.PosePosition = math.Approach( self.PosePosition, TargetPos, FrameTime() * 1 )
			self:SetPoseParameter( "switch", self.PosePosition )
			self:InvalidateBoneCache()
		end
		
	end
end