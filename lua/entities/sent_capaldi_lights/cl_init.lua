include('shared.lua')

function ENT:Draw()
	if LocalPlayer().capaldi==self:GetNWEntity("capaldi", NULL) and LocalPlayer().capaldi_viewmode and not LocalPlayer().capaldi_render then
		self:DrawModel()
	end
end

function ENT:Initialize()
	self.PosePosition = 0.5
end

function ENT:Think()
	local capaldi=self:GetNWEntity("capaldi",NULL)
	if IsValid(capaldi) and LocalPlayer().capaldi_viewmode and LocalPlayer().capaldi==capaldi then
			if capaldi.health and capaldi.health > 0 and not capaldi.repairing and capaldi.power then
	                          self:SetColor(Color(255,255,255))
                        else
                                  self:SetColor(Color(50,50,50))
			end
        end
end