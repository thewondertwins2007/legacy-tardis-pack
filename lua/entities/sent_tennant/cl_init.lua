include('shared.lua')
 
--[[---------------------------------------------------------
   Name: Draw
   Purpose: Draw the model in-game.
   Remember, the things you render first will be underneath!
---------------------------------------------------------]]
function ENT:Draw() 
	if not self.phasing and self.visible and not self.invortex then
		self:DrawModel()
		if WireLib then
			Wire_Render(self)
		end
	elseif self.phasing then
		if self.percent then
			if not self.phasemode and self.highPer <= 0 then
				self.phasing=false
			elseif self.phasemode and self.percent >= 1 then
				self.phasing=false
			end
		end
		
		self.percent = (self.phaselifetime - CurTime())
		self.highPer = self.percent + 0.5
		if self.phasemode then
			self.percent = (1-self.percent)-0.5
			self.highPer = self.percent+0.5
		end
		self.percent = math.Clamp( self.percent, 0, 1 )
		self.highPer = math.Clamp( self.highPer, 0, 1 )

		--Drawing original model
		local normal = self:GetUp()
		local origin = self:GetPos() + self:GetUp() * (self.maxs.z - ( self.height * self.highPer ))
		local distance = normal:Dot( origin )
		
		render.EnableClipping( true )
		render.PushCustomClipPlane( normal, distance )
			self:DrawModel()
		render.PopCustomClipPlane()
		
		local restoreT = self:GetMaterial()
		
		--Drawing phase texture
		render.MaterialOverride( self.wiremat )

		normal = self:GetUp()
		distance = normal:Dot( origin )
		render.PushCustomClipPlane( normal, distance )
		
		local normal2 = self:GetUp() * -1
		local origin2 = self:GetPos() + self:GetUp() * (self.maxs.z - ( self.height * self.percent ))
		local distance2 = normal2:Dot( origin2 )
		render.PushCustomClipPlane( normal2, distance2 )
			self:DrawModel()
		render.PopCustomClipPlane()
		render.PopCustomClipPlane()
		
		render.MaterialOverride( restoreT )
		render.EnableClipping( false )
	end
end

function ENT:Phase(mode)
	self.phasing=true
	self.phaseactive=true
	self.phaselifetime=CurTime()+1
	self.phasemode=mode
end

function ENT:Initialize()
	self.health=100
	self.phasemode=false
	self.visible=true
	self.flightmode=false
	self.visible=true
	self.power=true
	self.z=0
	self.phasedraw=0
	self.mins = self:OBBMins()
	self.maxs = self:OBBMaxs()
	self.wiremat = Material( "models/vtalanov98old/tennant/phase" )
	self.height = self.maxs.z - self.mins.z
end

function ENT:OnRemove()
	if self.flightloop then
		self.flightloop:Stop()
		self.flightloop=nil
	end
	if self.flightloop2 then
		self.flightloop2:Stop()
		self.flightloop2=nil
	end
end

function ENT:Think()
	if tobool(GetConVarNumber("tennant_flightsound"))==true then
		if not self.flightloop then
			self.flightloop=CreateSound(self, "vtalanov98old/tennant/flight_loop.wav")
			self.flightloop:SetSoundLevel(90)
			self.flightloop:Stop()
		end
		if self.flightmode and self.visible and not self.moving then
			if !self.flightloop:IsPlaying() then
				self.flightloop:Play()
			end
			local e = LocalPlayer():GetViewEntity()
			if !IsValid(e) then e = LocalPlayer() end
			local tennant=LocalPlayer().tennant
			if not (tennant and IsValid(tennant) and tennant==self and e==LocalPlayer()) then
				local pos = e:GetPos()
				local spos = self:GetPos()
				local doppler = (pos:Distance(spos+e:GetVelocity())-pos:Distance(spos+self:GetVelocity()))/200
				if self.exploded then
					local r=math.random(90,130)
					self.flightloop:ChangePitch(math.Clamp(r+doppler,80,120),0.1)
				else
					self.flightloop:ChangePitch(math.Clamp((self:GetVelocity():Length()/250)+95+doppler,80,120),0.1)
				end
				self.flightloop:ChangeVolume(GetConVarNumber("tennant_flightvol"),0)
			else
				if self.exploded then
					local r=math.random(90,130)
					self.flightloop:ChangePitch(r,0.1)
				else
					local p=math.Clamp(self:GetVelocity():Length()/250,0,15)
					self.flightloop:ChangePitch(95+p,0.1)
				end
				self.flightloop:ChangeVolume(0.75*GetConVarNumber("tennant_flightvol"),0)
			end
		else
			if self.flightloop:IsPlaying() then
				self.flightloop:Stop()
			end
		end
		
		local interior=self:GetNWEntity("interior",NULL)
		if not self.flightloop2 and interior and IsValid(interior) then
			self.flightloop2=CreateSound(interior, "vtalanov98old/tennant/flight_loop.wav")
			self.flightloop2:Stop()
		end
		if self.flightloop2 and (self.flightmode or self.invortex) and LocalPlayer().tennant_viewmode and not IsValid(LocalPlayer().tennant_skycamera) and interior and IsValid(interior) and ((self.invortex and self.moving) or not self.moving) then
			if !self.flightloop2:IsPlaying() then
				self.flightloop2:Play()
				self.flightloop2:ChangeVolume(0.4,0)
			end
			if self.exploded then
				local r=math.random(90,130)
				self.flightloop2:ChangePitch(r,0.1)
			else
				local p=math.Clamp(self:GetVelocity():Length()/250,0,15)
				self.flightloop2:ChangePitch(95+p,0.1)
			end
		elseif self.flightloop2 then
			if self.flightloop2:IsPlaying() then
				self.flightloop2:Stop()
			end
		end
	else
		if self.flightloop then
			self.flightloop:Stop()
			self.flightloop=nil
		end
		if self.flightloop2 then
			self.flightloop2:Stop()
			self.flightloop2=nil
		end
	end
	
	if self.light_on and tobool(GetConVarNumber("tennant_dynamiclight"))==true then
		local dlight = DynamicLight( self:EntIndex() )
		if ( dlight ) then
			local size=400
			local v=self:GetNWVector("extcol",Vector(255,255,255))
			local c=Color(v.x,v.y,v.z)
			dlight.Pos = self:GetPos() + self:GetUp() * 120
			dlight.r = c.r
			dlight.g = c.g
			dlight.b = c.b
			dlight.Brightness = 1
			dlight.Decay = size * 5
			dlight.Size = size
			dlight.DieTime = CurTime() + 1
		end
	end
	if self.health and self.health > 20 and self.power and self.visible and not self.moving and tobool(GetConVarNumber("tennant_dynamiclight"))==true then
		local dlight2 = DynamicLight( self:EntIndex() )
		if ( dlight2 ) then
			local size=400
			local v=self:GetNWVector("extcol",Vector(255,255,255))
			local c=Color(v.x,v.y,v.z)
			dlight2.Pos = self:GetPos() + self:GetUp() * 120
			dlight2.r = c.r
			dlight2.g = c.g
			dlight2.b = c.b
			dlight2.Brightness = 1
			dlight2.Decay = size * 5
			dlight2.Size = size
			dlight2.DieTime = CurTime() + 1
		end
	end
end

net.Receive("tennant-UpdateVis", function()
	local ent=net.ReadEntity()
	ent.visible=tobool(net.ReadBit())
end)

net.Receive("tennant-Phase", function()
	local tennant=net.ReadEntity()
	local interior=net.ReadEntity()
	if IsValid(tennant) then
		tennant.visible=tobool(net.ReadBit())
		tennant:Phase(tennant.visible)
		if not tennant.visible and tobool(GetConVarNumber("tennant_phasesound"))==true then
			tennant:EmitSound("vtalanov98old/tennant/phase_enable.wav", 100, 100)
			if IsValid(interior) then
				interior:EmitSound("vtalanov98old/tennant/phase_enable.wav", 100, 100)
			end
		end
	end
end)

net.Receive("tennant-Explode", function()
	local ent=net.ReadEntity()
	ent.exploded=true
end)

net.Receive("tennant-UnExplode", function()
	local ent=net.ReadEntity()
	ent.exploded=false
end)

net.Receive("tennant-Flightmode", function()
	local ent=net.ReadEntity()
	ent.flightmode=tobool(net.ReadBit())
end)

net.Receive("tennant-SetInterior", function()
	local ent=net.ReadEntity()
	ent.interior=net.ReadEntity()
end)

local tpsounds={}
tpsounds[0]={ // normal
	"vtalanov98old/tennant/demat.wav",
	"vtalanov98old/tennant/mat.wav",
	"vtalanov98old/tennant/full.wav"
}
tpsounds[1]={ // fast demat
	"vtalanov98old/tennant/fast_demat.wav",
	"vtalanov98old/tennant/mat.wav",
	"vtalanov98old/tennant/full.wav"
}
tpsounds[2]={ // fast return
	"vtalanov98old/tennant/fastreturn_demat.wav",
	"vtalanov98old/tennant/fastreturn_mat.wav",
	"vtalanov98old/tennant/fastreturn_full.wav"
}
net.Receive("tennant-Go", function()
	local tennant=net.ReadEntity()
	if IsValid(tennant) then
		tennant.moving=true
	end
	local interior=net.ReadEntity()
	local exploded=tobool(net.ReadBit())
	local pitch=(exploded and 110 or 100)
	local long=tobool(net.ReadBit())
	local snd=net.ReadFloat()
	local snds=tpsounds[snd]
	if tobool(GetConVarNumber("tennant_matsound"))==true then
		if IsValid(tennant) and LocalPlayer().tennant==tennant then
			if tennant.visible then
				if long then
					tennant:EmitSound(snds[1], 100, pitch)
				else
					tennant:EmitSound(snds[3], 100, pitch)
				end
			end
			if interior and IsValid(interior) and LocalPlayer().tennant_viewmode and not IsValid(LocalPlayer().tennant_skycamera) then
				if long then
				sound.Play("vtalanov98old/tennant/demat.wav", interior:LocalToWorld(Vector(0,0,90)))
				else
				sound.Play("vtalanov98old/tennant/full.wav", interior:LocalToWorld(Vector(0,0,90)))
				end
			end
		elseif IsValid(tennant) and tennant.visible then
			local pos=net.ReadVector()
			local pos2=net.ReadVector()
			if pos then
				sound.Play(snds[1], pos, 75, pitch)
			end
			if pos2 and not long then
				sound.Play(snds[2], pos2, 75, pitch)
			end
		end
	end
end)

net.Receive("tennant-Stop", function()
	tennant=net.ReadEntity()
	if IsValid(tennant) then
		tennant.moving=nil
	end
end)

net.Receive("tennant-Reappear", function()
	local tennant=net.ReadEntity()
	local interior=net.ReadEntity()
	local exploded=tobool(net.ReadBit())
	local pitch=(exploded and 110 or 100)
	local snd=net.ReadFloat()
	local snds=tpsounds[snd]
	if tobool(GetConVarNumber("tennant_matsound"))==true then
		if IsValid(tennant) and LocalPlayer().tennant==tennant then
			if tennant.visible then
				tennant:EmitSound(snds[2], 100, pitch)
			end
			if interior and IsValid(interior) and LocalPlayer().tennant_viewmode and not IsValid(LocalPlayer().tennant_skycamera) then
				sound.Play("vtalanov98old/tennant/mat.wav", interior:LocalToWorld(Vector(0,0,90)))
			end
		elseif IsValid(tennant) and tennant.visible then
			sound.Play(snds[2], net.ReadVector(), 75, pitch)
		end
	end
end)

net.Receive("Player-Settennant", function()
	local ply=net.ReadEntity()
	ply.tennant=net.ReadEntity()
end)

net.Receive("tennant-SetHealth", function()
	local tennant=net.ReadEntity()
	tennant.health=net.ReadFloat()
end)

net.Receive("tennant-SetLocked", function()
	local tennant=net.ReadEntity()
	local interior=net.ReadEntity()
	local locked=tobool(net.ReadBit())
	local makesound=tobool(net.ReadBit())
	if IsValid(tennant) then
		tennant.locked=locked
		if tobool(GetConVarNumber("tennant_locksound"))==true and makesound then
			sound.Play("vtalanov98old/tennant/lock.wav", tennant:GetPos())
		end
	end
	if IsValid(interior) then
		if tobool(GetConVarNumber("tennant_locksound"))==true and not IsValid(LocalPlayer().tennant_skycamera) and makesound then
			sound.Play("vtalanov98old/tennant/lock.wav", interior:LocalToWorld(Vector(308,0,80)))
		end
	end
end)

net.Receive("tennant-SetViewmode", function()
	LocalPlayer().tennant_viewmode=tobool(net.ReadBit())
	LocalPlayer().ShouldDisableLegs=(not LocalPlayer().tennant_viewmode)
	
	if LocalPlayer().tennant_viewmode and GetConVarNumber("r_rootlod")>0 then
		Derma_Query("The TARDIS Interior requires model detail on high, set now?", "TARDIS Interior", "Yes", function() RunConsoleCommand("r_rootlod", 0) end, "No", function() end)
	end
		
end)

hook.Add( "ShouldDrawLocalPlayer", "tennant-ShouldDrawLocalPlayer", function(ply)
	if IsValid(ply.tennant) and not ply.tennant_viewmode then
		return false
	end
end)

net.Receive("tennant-PlayerEnter", function()
	if tobool(GetConVarNumber("tennant_doorsound"))==true then
		local ent1=net.ReadEntity()
		local ent2=net.ReadEntity()
		if IsValid(ent1) and ent1.visible then
			sound.Play("vtalanov98old/tennant/door.wav", ent1:GetPos())
		end
		if IsValid(ent2) and not IsValid(LocalPlayer().tennant_skycamera) then
			sound.Play("vtalanov98old/tennant/door.wav", ent2:LocalToWorld(Vector(308,0,80)))
		end
	end
end)

net.Receive("tennant-PlayerExit", function()
	if tobool(GetConVarNumber("tennant_doorsound"))==true then
		local ent1=net.ReadEntity()
		local ent2=net.ReadEntity()
		if IsValid(ent1) and ent1.visible then
			sound.Play("vtalanov98old/tennant/door.wav", ent1:GetPos())
		end
		if IsValid(ent2) and not IsValid(LocalPlayer().tennant_skycamera) then
			sound.Play("vtalanov98old/tennant/door.wav", ent2:LocalToWorld(Vector(308,0,80)))
		end
	end
end)

net.Receive("tennant-SetRepairing", function()
	local tennant=net.ReadEntity()
	local repairing=tobool(net.ReadBit())
	local interior=net.ReadEntity()
	if IsValid(tennant) then
		tennant.repairing=repairing
	end
	if IsValid(interior) and LocalPlayer().tennant==tennant and LocalPlayer().tennant_viewmode and tobool(GetConVarNumber("tennantint_powersound"))==true then
		if repairing then
			sound.Play("vtalanov98old/tennant/powerdown.wav", interior:GetPos())
		else
			sound.Play("vtalanov98old/tennant/powerup.wav", interior:GetPos())
		end
	end
end)

net.Receive("tennant-BeginRepair", function()
	local tennant=net.ReadEntity()
	if IsValid(tennant) then
		/*
		local mat=Material("models/vtalanov98old/tennant/2005")
		if not mat:IsError() then
			mat:SetTexture("$basetexture", "models/props_combine/metal_combinebridge001")
		end
		*/
	end
end)

net.Receive("tennant-FinishRepair", function()
	local tennant=net.ReadEntity()
	if IsValid(tennant) then
		if tobool(GetConVarNumber("tennantint_repairsound"))==true and tennant.visible then
			sound.Play("vtalanov98old/tennant/repairfinish.wav", tennant:GetPos())
		end
		/*
		local mat=Material("models/vtalanov98old/tennant/2005")
		if not mat:IsError() then
			mat:SetTexture("$basetexture", "models/vtalanov98old/tennant/2005")
		end
		*/
	end
end)

net.Receive("tennant-SetLight", function()
	local tennant=net.ReadEntity()
	local on=tobool(net.ReadBit())
	if IsValid(tennant) then
		tennant.light_on=on
	end
end)

net.Receive("tennant-SetPower", function()
	local tennant=net.ReadEntity()
	local on=tobool(net.ReadBit())
	local interior=net.ReadEntity()
	if IsValid(tennant) then
		tennant.power=on
	end
	if IsValid(interior) and LocalPlayer().tennant==tennant and LocalPlayer().tennant_viewmode and tobool(GetConVarNumber("tennantint_powersound"))==true then
		if on then
			sound.Play("vtalanov98old/tennant/powerup.wav", interior:GetPos())
		else
			sound.Play("vtalanov98old/tennant/powerdown.wav", interior:GetPos())
		end
	end
end)

net.Receive("tennant-SetVortex", function()
	local tennant=net.ReadEntity()
	local on=tobool(net.ReadBit())
	if IsValid(tennant) then
		tennant.invortex=on
	end
end)

surface.CreateFont( "HUDNumber", {font="Trebuchet MS", size=40, weight=900} )

hook.Add("HUDPaint", "tennant-DrawHUD", function()
	local p = LocalPlayer()
	local tennant = p.tennant
	if tennant and IsValid(tennant) and tennant.health and (tobool(GetConVarNumber("tennant_takedamage"))==true or tennant.exploded) then
		local health = math.floor(tennant.health)
		local n=0
		if health <= 99 then
			n=20
		end
		if health <= 9 then
			n=40
		end
		local col=Color(255,255,255)
		if health <= 20 then
			col=Color(255,0,0)
		end
		draw.RoundedBox( 0, 5, ScrH()-55, 220-n, 50, Color(0, 0, 0, 180) )
		draw.DrawText("Health: "..health.."%","HUDNumber", 15, ScrH()-52, col)
	end
end)

hook.Add("CalcView", "tennant_CLView", function( ply, origin, angles, fov )
	local tennant=LocalPlayer().tennant
	local viewent = LocalPlayer():GetViewEntity()
	if !IsValid(viewent) then viewent = LocalPlayer() end
	local dist= -300
	
	if tennant and IsValid(tennant) and viewent==LocalPlayer() and not LocalPlayer().tennant_viewmode then
		local pos=tennant:GetPos()+(tennant:GetUp()*50)
		local tracedata={}
		tracedata.start=pos
		tracedata.endpos=pos+ply:GetAimVector():GetNormal()*dist
		tracedata.mask=MASK_NPCWORLDSTATIC
		local trace=util.TraceLine(tracedata)
		local view = {}
		view.origin = trace.HitPos
		view.angles = angles
		return view
	end
end)

hook.Add( "HUDShouldDraw", "tennant-HideHUD", function(name)
	local viewmode=LocalPlayer().tennant_viewmode
	if ((name == "CHudHealth") or (name == "CHudBattery")) and viewmode then
		return false
	end
end)