include('shared.lua')
 
--[[---------------------------------------------------------
   Name: Draw
   Purpose: Draw the model in-game.
   Remember, the things you render first will be underneath!
---------------------------------------------------------]]
function ENT:Draw()
	if LocalPlayer().hartnell_viewmode and self:GetNWEntity("hartnell",NULL)==LocalPlayer().hartnell and not LocalPlayer().hartnell_render then
		self:DrawModel()
		if WireLib then
			Wire_Render(self)
		end
	end
end

function ENT:OnRemove()
	if self.cloisterbell then
		self.cloisterbell:Stop()
		self.cloisterbell=nil
	end
	if self.idlesound then
		self.idlesound:Stop()
		self.idlesound=nil
	end
end

function ENT:Initialize()
	self.timerotor={}
	self.timerotor.pos=0
	self.timerotor.mode=1
	self.parts={}
end

net.Receive("hartnellInt-SetParts", function()
	local t={}
	local interior=net.ReadEntity()
	local count=net.ReadFloat()
	for i=1,count do
		local ent=net.ReadEntity()
		ent.hartnell_part=true
		if IsValid(interior) then
			table.insert(interior.parts,ent)
		end
	end
end)

net.Receive("hartnellInt-UpdateAdv", function()
	local success=tobool(net.ReadBit())
	if success then
	else
		surface.PlaySound("vtalanov98old/hartnell/cloister.wav")
	end
end)

net.Receive("hartnellInt-SetAdv", function()
	local interior=net.ReadEntity()
	local ply=net.ReadEntity()
	local mode=net.ReadFloat()
	if IsValid(interior) and IsValid(ply) and mode then
		if ply==LocalPlayer() then
		end
		interior.flightmode=mode
	end
end)

net.Receive("hartnellInt-ControlSound", function()
	local hartnell=net.ReadEntity()
	local control=net.ReadEntity()
	local snd=net.ReadString()
	if IsValid(hartnell) and IsValid(control) and snd and tobool(GetConVarNumber("hartnellint_controlsound"))==true and LocalPlayer().hartnell==hartnell and LocalPlayer().hartnell_viewmode then
		sound.Play(snd,control:GetPos())
	end
end)

function ENT:Think()
	local hartnell=self:GetNWEntity("hartnell",NULL)
	if IsValid(hartnell) and LocalPlayer().hartnell_viewmode and LocalPlayer().hartnell==hartnell then
		if tobool(GetConVarNumber("hartnellint_cloisterbell"))==true and not IsValid(LocalPlayer().hartnell_skycamera) then
			if hartnell.health and hartnell.health < 21 then
				if self.cloisterbell and !self.cloisterbell:IsPlaying() then
					self.cloisterbell:Play()
				elseif not self.cloisterbell then
					self.cloisterbell = CreateSound(self, "vtalanov98old/hartnell/cloisterbell_loop.wav")
					self.cloisterbell:Play()
				end
			else
				if self.cloisterbell and self.cloisterbell:IsPlaying() then
					self.cloisterbell:Stop()
					self.cloisterbell=nil
				end
			end
		else
			if self.cloisterbell and self.cloisterbell:IsPlaying() then
				self.cloisterbell:Stop()
				self.cloisterbell=nil
			end
		end
		
		if tobool(GetConVarNumber("hartnellint_idlesound"))==true and hartnell.health and hartnell.health >= 1 and not IsValid(LocalPlayer().hartnell_skycamera) and not hartnell.repairing and hartnell.power then
			if self.idlesound and !self.idlesound:IsPlaying() then
				self.idlesound:Play()
			elseif not self.idlesound then
				self.idlesound = CreateSound(self, "vtalanov98old/hartnell/interior_idle_loop.wav")
				self.idlesound:Play()
				self.idlesound:ChangeVolume(0.5,0)
			end
		else
			if self.idlesound and self.idlesound:IsPlaying() then
				self.idlesound:Stop()
				self.idlesound=nil
			end
		end
		
		if not IsValid(LocalPlayer().hartnell_skycamera) and tobool(GetConVarNumber("hartnellint_dynamiclight"))==true then
			if hartnell.health and hartnell.health > 0 and not hartnell.repairing and hartnell.power then
				local dlight = DynamicLight( self:EntIndex() )
				if ( dlight ) then
					local size=1024
					local v=self:GetNWVector("mainlight",Vector(0,0,300))
					if hartnell.health < 21 then
						v=self:GetNWVector("warnlight",Vector(0,0,300))
					end
					dlight.Pos = self:LocalToWorld(Vector(0,0,300))
					dlight.r = v.x
					dlight.g = v.y
					dlight.b = v.z
					dlight.Brightness = 2
					dlight.Decay = size * 5
					dlight.Size = size
					dlight.DieTime = CurTime() + 1
				end
			end
			
			
			if (hartnell.moving or hartnell.flightmode) then
				if self.timerotor.pos==1 then
					self.timerotor.pos=0
				end
				
				self.timerotor.pos=math.Approach( self.timerotor.pos, self.timerotor.mode, FrameTime()*0.2 )
				self:SetPoseParameter( "glass", self.timerotor.pos )
			end
		end
	else
		if self.cloisterbell then
			self.cloisterbell:Stop()
			self.cloisterbell=nil
		end
		if self.idlesound then
			self.idlesound:Stop()
			self.idlesound=nil
		end
	end
end