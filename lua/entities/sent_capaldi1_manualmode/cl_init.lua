include('shared.lua')

function ENT:Draw()
	if LocalPlayer().capaldi1==self:GetNWEntity("capaldi1", NULL) and LocalPlayer().capaldi1_viewmode and not LocalPlayer().capaldi1_render then
		self:DrawModel()
	end
end
function ENT:Think()
	local capaldi1=self:GetNWEntity("capaldi1",NULL)
	if IsValid(capaldi1) and LocalPlayer().capaldi1_viewmode and LocalPlayer().capaldi1==capaldi1 then
		if not capaldi1.power or capaldi1.repairing then
			self:SetMaterial("models/cem111333/capaldi1/telepathicoff")
		else
			self:SetMaterial("models/cem111333/capaldi1/telepathic")
			if capaldi1.health < 21 then
				self:SetMaterial("models/cem111333/capaldi1/telepathicdmg")
			end
		end
        end
end